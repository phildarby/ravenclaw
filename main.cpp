/*
 * Software created by:
 * Michael Champagne
 * Raven (ID: 22885233 | 512388)

 * Licensed with GNU lpgl v3.0
 * Found in gnu-v3.txt or available online here
 * http://www.gnu.org/licenses/lgpl.txt
 */

/* Credits
 * Ross (for the original php framework)
 * Folks on palringo (for new auth procedure)
 * Current palringo staff members are douchebags
 * Nom helped alot and so did sniper/caelean
 */

#include "main.h"

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        //get base filename not full argv[0] path
        char *filePath = strrchr(argv[0], '\\');
        string trueName = filePath;
        trueName.erase(0, 1);

        cout << "Bot Cannot start\n"
                "Logon details are loaded from an ini file\n\n"
                "Usage: "<< trueName <<" [file.ini]\r\n"
                "An example file has been created for you\r\nAlong with an example batch script to start the bot properly" << endl;

        string iniFile  = "example.ini";
        string iniFile1  = "triviaplayersId.bin";
        string iniFile2  = "triviaplayersName.bin";
        string triviaScore = "Trivia.ini";
        string batch    = "runBot.bat";

        //deletes preexisting example file
        remove(iniFile.c_str());
        remove(batch.c_str());

        //creates a super basic ini filee
        ofstream iniStream;
        iniStream.open(iniFile.c_str(), ios::out | ios::app | ios::binary);
        iniStream <<    "[LOGON]\r\n"
                        "#your palringo logon details\r\n"
                        "email=example@email.com\r\n"
                        "password=123456\r\n"
                        "REQUIRED it's used in one or two applications currently to prevent the bot 'talking to itself'\r\n"
                        "botId=123456\r\n"
                        "#SSL yes or no\r\n"
                        "HTTPS=no\r\n\r\n"
                        "[SETTINGS]\r\n"
                        "#the user ID of the user in chage of the bot\r\n"
                        "adminId=1234\r\n\n"
                        "#A plain text name for the admin\r\n"
                        "adminName=myName\r\n\n"
                        "#A plain text name for the bot to respond tor\n"
                        "botName=ravenclaw\r\n\n"
                        "#the prefix for admin bot commands ie: #join or #leave\r\n"
                        "cmdAdmin=#\r\n\n"
                        "#the prefix for user bot commands ie: /help or /google\r\n"
                        "cmdUser=/\r\n"
                        "#Required for an offical palringo bot turns [#help] into [#rc help]\r\n"
                        "#Namespace can be anything as long as it has no spaces\r\n"
                        "nameSpace=rc\r\n"
                        "#if you delete the namespace field the bot will work as normal ie: '#help'\r\n"
                        "#Please see readme.html for more ini settings"
                        ;
        iniStream.close();

        //creates the batch script
        iniStream.open(batch.c_str(), ios::out | ios::app | ios::binary);
        iniStream << trueName << " example.ini";
        iniStream.close();

        engine.pause();
        return 1;
    }
    else if(argc == 2)
    {
        string iniFile = argv[1];
        string iniFile1  = "triviaplayersId.bin";
        string iniFile2  = "triviaplayersName.bin";
        INIReader reader = INIReader(iniFile);



        if (reader.ParseError() < 0)
        {
            cout << "Can't load " << iniFile << "\n";
            return 1;
        }




        map<string, string> botSettings;

        //get LOGON details
        botSettings["username"]     = reader.Get("LOGON", "email", "UNKNOWN");
        botSettings["password"]     = reader.Get("LOGON", "password", "UNKNOWN");
        botSettings["botId"]        = reader.Get("LOGON", "botId", "UNKNOWN");
        botSettings["SSL"]        = reader.Get("LOGON", "HTTPS", "true");

        //get bot settings
        botSettings["botAdmin"]     = reader.Get("SETTINGS", "adminId", "UNKNOWN");
        botSettings["adminName"]    = reader.Get("SETTINGS", "adminName", "UNKNOWN");
        botSettings["botName"]      = reader.Get("SETTINGS", "botName", "UNKNOWN");
        botSettings["cmdAdmin"]     = reader.Get("SETTINGS", "cmdAdmin", "#");
        botSettings["cmdUser"]      = reader.Get("SETTINGS", "cmdUser", "/");
        botSettings["nameSpace"]    = reader.Get("SETTINGS", "nameSpace", "UNKNOWN");
        botSettings["botGroupName"]    = reader.Get("SETTINGS", "botGroupName", "UNKNOWN");
        botSettings["botGroupID"]    = reader.Get("SETTINGS", "botGroupID", "UNKNOWN");

        //get question details
        botSettings["question1"]     = reader.Get("QUESTIONS", "question1", "UNKNOWN");
        botSettings["question2"]     = reader.Get("QUESTIONS", "question2", "UNKNOWN");
        botSettings["question3"]        = reader.Get("QUESTIONS", "question3", "UNKNOWN");
        botSettings["question4"]        = reader.Get("QUESTIONS", "question4", "UNKNOWN");
        botSettings["question5"]     = reader.Get("QUESTIONS", "question5", "UNKNOWN");
        botSettings["question6"]     = reader.Get("QUESTIONS", "question6", "UNKNOWN");
        botSettings["question7"]        = reader.Get("QUESTIONS", "question7", "UNKNOWN");
        botSettings["question8"]        = reader.Get("QUESTIONS", "question8", "UNKNOWN");
        botSettings["question9"]     = reader.Get("QUESTIONS", "question9", "UNKNOWN");
        botSettings["question10"]     = reader.Get("QUESTIONS", "question10", "UNKNOWN");
        botSettings["question11"]     = reader.Get("QUESTIONS", "question11", "UNKNOWN");
        botSettings["question12"]     = reader.Get("QUESTIONS", "question12", "UNKNOWN");
        botSettings["question13"]        = reader.Get("QUESTIONS", "question13", "UNKNOWN");
        botSettings["question14"]        = reader.Get("QUESTIONS", "question14", "UNKNOWN");
        botSettings["question15"]     = reader.Get("QUESTIONS", "question15", "UNKNOWN");
        botSettings["question16"]     = reader.Get("QUESTIONS", "question16", "UNKNOWN");
        botSettings["question17"]        = reader.Get("QUESTIONS", "question17", "UNKNOWN");
        botSettings["question18"]        = reader.Get("QUESTIONS", "question18", "UNKNOWN");
        botSettings["question19"]     = reader.Get("QUESTIONS", "question19", "UNKNOWN");
        botSettings["question20"]     = reader.Get("QUESTIONS", "question20", "UNKNOWN");
        botSettings["question21"]     = reader.Get("QUESTIONS", "question21", "UNKNOWN");
        botSettings["question22"]     = reader.Get("QUESTIONS", "question22", "UNKNOWN");
        botSettings["question23"]        = reader.Get("QUESTIONS", "question23", "UNKNOWN");
        botSettings["question24"]        = reader.Get("QUESTIONS", "question24", "UNKNOWN");
        botSettings["question25"]     = reader.Get("QUESTIONS", "question25", "UNKNOWN");
        botSettings["question26"]     = reader.Get("QUESTIONS", "question26", "UNKNOWN");
        botSettings["question27"]        = reader.Get("QUESTIONS", "question27", "UNKNOWN");
        botSettings["question28"]        = reader.Get("QUESTIONS", "question28", "UNKNOWN");
        botSettings["question29"]     = reader.Get("QUESTIONS", "question29", "UNKNOWN");
        botSettings["question30"]     = reader.Get("QUESTIONS", "question30", "UNKNOWN");
        botSettings["question31"]     = reader.Get("QUESTIONS", "question31", "UNKNOWN");
        botSettings["question32"]     = reader.Get("QUESTIONS", "question32", "UNKNOWN");
        botSettings["question33"]        = reader.Get("QUESTIONS", "question33", "UNKNOWN");
        botSettings["question34"]        = reader.Get("QUESTIONS", "question34", "UNKNOWN");
        botSettings["question35"]     = reader.Get("QUESTIONS", "question35", "UNKNOWN");
        botSettings["question36"]     = reader.Get("QUESTIONS", "question36", "UNKNOWN");
        botSettings["question37"]        = reader.Get("QUESTIONS", "question37", "UNKNOWN");
        botSettings["question38"]        = reader.Get("QUESTIONS", "question38", "UNKNOWN");
        botSettings["question39"]     = reader.Get("QUESTIONS", "question39", "UNKNOWN");
        botSettings["question40"]     = reader.Get("QUESTIONS", "question40", "UNKNOWN");
        botSettings["question41"]     = reader.Get("QUESTIONS", "question41", "UNKNOWN");
        botSettings["question42"]     = reader.Get("QUESTIONS", "question42", "UNKNOWN");
        botSettings["question43"]        = reader.Get("QUESTIONS", "question43", "UNKNOWN");
        botSettings["question44"]        = reader.Get("QUESTIONS", "question44", "UNKNOWN");
        botSettings["question45"]     = reader.Get("QUESTIONS", "question45", "UNKNOWN");
        botSettings["question46"]     = reader.Get("QUESTIONS", "question46", "UNKNOWN");
        botSettings["question47"]        = reader.Get("QUESTIONS", "question47", "UNKNOWN");
        botSettings["question48"]        = reader.Get("QUESTIONS", "question48", "UNKNOWN");
        botSettings["question49"]     = reader.Get("QUESTIONS", "question49", "UNKNOWN");
        botSettings["question50"]     = reader.Get("QUESTIONS", "question50", "UNKNOWN");


        //get answer details
        botSettings["answer1"]     = reader.Get("ANSWERS", "answer1", "UNKNOWN");
        botSettings["answer2"]     = reader.Get("ANSWERS", "answer2", "UNKNOWN");
        botSettings["answer3"]     = reader.Get("ANSWERS", "answer3", "UNKNOWN");
        botSettings["answer4"]        = reader.Get("ANSWERS", "answer4", "UNKNOWN");
        botSettings["answer5"]        = reader.Get("ANSWERS", "answer5", "UNKNOWN");
        botSettings["answer6"]     = reader.Get("ANSWERS", "answer6", "UNKNOWN");
        botSettings["answer7"]     = reader.Get("ANSWERS", "answer7", "UNKNOWN");
        botSettings["answer8"]        = reader.Get("ANSWERS", "answer8", "UNKNOWN");
        botSettings["answer9"]        = reader.Get("ANSWERS", "answer9", "UNKNOWN");
        botSettings["answer10"]     = reader.Get("ANSWERS", "answer10", "UNKNOWN");
        botSettings["answer11"]     = reader.Get("ANSWERS", "answer11", "UNKNOWN");
        botSettings["answer12"]     = reader.Get("ANSWERS", "answer12", "UNKNOWN");
        botSettings["answer13"]     = reader.Get("ANSWERS", "answer13", "UNKNOWN");
        botSettings["answer14"]        = reader.Get("ANSWERS", "answer14", "UNKNOWN");
        botSettings["answer15"]        = reader.Get("ANSWERS", "answer15", "UNKNOWN");
        botSettings["answer16"]     = reader.Get("ANSWERS", "answer16", "UNKNOWN");
        botSettings["answer17"]     = reader.Get("ANSWERS", "answer17", "UNKNOWN");
        botSettings["answer18"]        = reader.Get("ANSWERS", "answer18", "UNKNOWN");
        botSettings["answer19"]        = reader.Get("ANSWERS", "answer19", "UNKNOWN");
        botSettings["answer20"]     = reader.Get("ANSWERS", "answer20", "UNKNOWN");
        botSettings["answer21"]     = reader.Get("ANSWERS", "answer21", "UNKNOWN");
        botSettings["answer22"]     = reader.Get("ANSWERS", "answer22", "UNKNOWN");
        botSettings["answer23"]     = reader.Get("ANSWERS", "answer23", "UNKNOWN");
        botSettings["answer24"]        = reader.Get("ANSWERS", "answer24", "UNKNOWN");
        botSettings["answer25"]        = reader.Get("ANSWERS", "answer25", "UNKNOWN");
        botSettings["answer26"]     = reader.Get("ANSWERS", "answer26", "UNKNOWN");
        botSettings["answer27"]     = reader.Get("ANSWERS", "answer27", "UNKNOWN");
        botSettings["answer28"]        = reader.Get("ANSWERS", "answer28", "UNKNOWN");
        botSettings["answer29"]        = reader.Get("ANSWERS", "answer29", "UNKNOWN");
        botSettings["answer30"]     = reader.Get("ANSWERS", "answer30", "UNKNOWN");
        botSettings["answer31"]     = reader.Get("ANSWERS", "answer31", "UNKNOWN");
        botSettings["answer32"]     = reader.Get("ANSWERS", "answer32", "UNKNOWN");
        botSettings["answer33"]     = reader.Get("ANSWERS", "answer33", "UNKNOWN");
        botSettings["answer34"]        = reader.Get("ANSWERS", "answer34", "UNKNOWN");
        botSettings["answer35"]        = reader.Get("ANSWERS", "answer35", "UNKNOWN");
        botSettings["answer36"]     = reader.Get("ANSWERS", "answer36", "UNKNOWN");
        botSettings["answer37"]     = reader.Get("ANSWERS", "answer37", "UNKNOWN");
        botSettings["answer38"]        = reader.Get("ANSWERS", "answer38", "UNKNOWN");
        botSettings["answer39"]        = reader.Get("ANSWERS", "answer39", "UNKNOWN");
        botSettings["answer40"]     = reader.Get("ANSWERS", "answer40", "UNKNOWN");
        botSettings["answer41"]     = reader.Get("ANSWERS", "answer41", "UNKNOWN");
        botSettings["answer42"]     = reader.Get("ANSWERS", "answer42", "UNKNOWN");
        botSettings["answer43"]     = reader.Get("ANSWERS", "answer43", "UNKNOWN");
        botSettings["answer44"]        = reader.Get("ANSWERS", "answer44", "UNKNOWN");
        botSettings["answer45"]        = reader.Get("ANSWERS", "answer45", "UNKNOWN");
        botSettings["answer46"]     = reader.Get("ANSWERS", "answer46", "UNKNOWN");
        botSettings["answer47"]     = reader.Get("ANSWERS", "answer47", "UNKNOWN");
        botSettings["answer48"]        = reader.Get("ANSWERS", "answer48", "UNKNOWN");
        botSettings["answer49"]        = reader.Get("ANSWERS", "answer49", "UNKNOWN");
        botSettings["answer50"]     = reader.Get("ANSWERS", "answer50", "UNKNOWN");

        //get player details
        botSettings["playerAccounts"]     = reader.Get("PLAYERS", "playerAccounts", "UNKNOWN");
        botSettings["playerNames"]     = reader.Get("PLAYERS", "playerNames", "UNKNOWN");
        botSettings["triviaCategory"] =  reader.Get("CATEGORY", "triviaCategory", "UNKNOWN");

		#ifdef RAVENCLAW_DEBUG
        //obtain user script settings
        botSettings["useScripts"]	= reader.Get("WEBSCRIPTS", "useWebScripts", "false");
        botSettings["postUrl"]		= reader.Get("WEBSCRIPTS", "postUrl", "UNKNOWN");
        #endif

        //Get the location of the bots ini file
        botSettings["iniFile"]      = iniFile;
        botSettings["iniFile1"]      = iniFile1;
        botSettings["iniFile2"]      = iniFile2;

        if(botSettings["username"]      == "UNKNOWN" ||
           botSettings["password"]      == "UNKNOWN" ||
           botSettings["botAdmin"]      == "UNKNOWN" ||
           botSettings["botName"]       == "UNKNOWN" ||
           botSettings["adminName"]     == "UNKNOWN" ||
           botSettings["botId"]     == "UNKNOWN" )
        {
            engine.pl("Bot cannot find required field(s) for logging on\r\nPlease check your .ini file");
            engine.pause();
            return 1;
        }
        #ifdef RAVENCLAW_DEBUG
        else if(botSettings["useScripts"] != "false" && botSettings["postUrl"] == "UNKNOWN")
		{
			engine.pl("You have enabled webScripts but have not supplied a URL please fix this issue.");
            engine.pause();
            return 1;
		}
		#endif




        INIReader triviaPlayerId = INIReader(iniFile1);

        if (triviaPlayerId.ParseError() < 0)
        {
            cout << "Can't load " << iniFile1 << "\n";
            return 1;
        }

        INIReader triviaPlayerName = INIReader(iniFile2);

        if (triviaPlayerName.ParseError() < 0)
        {
            cout << "Can't load " << iniFile2 << "\n";
            return 1;
        }




        spinUp(botSettings["botName"]);
        spinning(botSettings);
        spinDown();

        return 0;
    }
}

void spinUp(string botName)
{
    string titleName = "PalringoBot: "+botName;
	SetConsoleTitle( titleName.c_str() );
}

void spinning(map<string, string> botSettings)
{
	engine.pl("Main-> starting palClient", 1);
	palClient = new palringoClient(botSettings);
	palClient->run();
}

void spinDown()
{
	delete palClient;
	engine.pause();
}
