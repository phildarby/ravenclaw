#ifndef PALRINGODATABASE_H
#define PALRINGODATABASE_H

//project includes
#include "misc.h"
#include "crypt.h"
//#include "curl.h"

struct mod{
    string nickname;
	string userId;

};


class misc;
class crypt;
//class curl;
class palringoDatabase
{
    public:
        palringoDatabase();

        void modRegister(string user, string name);
        string modIdLookUp();               //looks up mod details based on ID
        string modNameLookUp();               //looks up mod details based on name
        map<string, string> modLookUp();

        void logAdminAction(string group, string admin, string user, string action);

        void userRegister(string user, string name);
        string userLookUp(string user);

        void groupRegister(string group, string name);
        string groupLookUp(string group);

        void logChat(string group, string user, string message);
    protected:
    private:
        misc	engine;
		crypt	cipher;
		map<string, mod> modList;
		//curl    Curl;
};

#endif // PALRINGODATABASE_H
