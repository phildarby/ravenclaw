/*
 * Software created by:
 * Michael Champagne
 * Raven (ID: 22885233 | 512388)

 * Licensed with GNU lpgl v3.0
 * Found in gnu-v3.txt or available online here
 * http://www.gnu.org/licenses/lgpl.txt
 */

/*	Class: baseClient
 *	Handles messages to and from palringo, with chatting, administration, etc etc
 */

#ifndef BASECLIENT_H
#define BASECLIENT_H

//library includes
#include <iostream>
#include <sstream>
#include <string>
#include <string.h>
#include <algorithm>
#include <vector>
#include <ctime>
#include <time.h>

//project includes
#include "misc.h"
#include "crypt.h"
//#include "curl.h"
#include "palringoMessage.h"
#include "palringoGroup.h"
#include "palringoContact.h"
#include "palringoDatabase.h"
#include "definitions.h"



struct botMod{
    string id;
    string name;
    string message;// = "";
    bool online;// = false;
};

struct botPlayer{
    string id;
    string name;

};

class misc;
class crypt;
//class curl;
class palringoMessage;
class palringoGroup;
class palringoContact;
class palringoDatabase;
class baseClient
{
	public:
		baseClient(map<string, string> botSettings);
		map<string, botPlayer> botPlayers;

		bool isPlayer(string userid);
		string searchPlayer(string playername);


		//The following 4 functions are called from palringoConnection when needed
		void recv_groupMessage(string group, string user, string message);
		void recv_personalMessage(string name, string user, string message);

		//group update and admin actions also dont touch
		void group_update(map<string, string> updatePacket);
		void pmModJoin(string group, string nickname);
		void group_admin(string group, string admin, string user, string action);

		//these functions should not be touched
		void	set_palMesg(palringoMessage *mesg);	//sets the palringoMessage pointer
		void	set_palGroup(palringoGroup *group);	//sets the palringoGroup pointer
		void	set_palContact(palringoContact *contact);	//sets the palringoContact pointer

        //returns pointers to componets
		palringoContact	*get_palContact();	//returns pointer to palContact

		string 	get_Username(void);	//returns username
		string 	get_Password(void);	//returns password

		//beta features
		void    parseResponse(packet response, packet input);

	protected:
		//Functions to send things
		void send_message(string group, string message);
		void send_pm(string id, string message);
		void send_image(string group, string image);
		void send_image_pm(string user, string image);

		//packet used for testing random things
		void send_debug(string to);

		//Group join and leave functions
		void group_join(string groupName, string password = "");
		void group_part(string groupID);

		//Self Explaining admin actions
		void admin_admin(string groupID, string userID);
		void admin_mod(string groupID, string userID);
		void admin_silence(string groupID, string userID);
		void admin_reset(string groupID, string userID);
		void admin_kick(string groupID, string userID);
		void admin_ban(string groupID, string userID);

		//"working" functions
		void    parse_groupMessage(string group, string user, vector<string> data);
		void    parse_personalMessage(string name, string user, vector<string> data);
		string  messagePatcher(vector<string> message, string patch = " ", int start = 2);
        void  parseTrivia(string group, string user, vector<string> data);
		void    reloadIni(string group = "");


	private:

		string	username, password, botAdmin, botName, adminName, adminMessage, botId, botGroupName, botGroupID;
		string  cmdAdmin, cmdBase, nameSpace, triviaCategory, question1, question2, question3, question4,
		 question5, question6, question7, question8,question9, question10,question11, question12, question13, question14,
		 question15, question16, question17, question18,question19, question20,question21, question22, question23, question24,
		 question25, question26, question27, question28,question29, question30,question31, question32, question33, question34,
		 question35, question36, question37, question38,question39, question40,question41, question42, question43, question44,
		 question45, question46, question47, question48,question49, question50,answer1, answer2, answer3, answer4,
		 answer5, answer6, answer7, answer8,answer9, answer10,answer11, answer12, answer13, answer14,
		 answer15, answer16, answer17, answer18,answer19, answer20,answer21, answer22, answer23, answer24,
		 answer25, answer26, answer27, answer28,answer29, answer30,answer31, answer32, answer33, answer34,
		 answer35, answer36, answer37, answer38,answer39, answer40,answer41, answer42, answer43, answer44,
		 answer45, answer46, answer47, answer48,answer49, answer50;
		bool	canTalk, adminOnline, security;
		time_t  startTime;
		string  startedTime;
		misc	engine;
		crypt	cipher;
		//curl    Curl;

		#ifdef RAVENCLAW_DEBUG
			palringoDatabase palDB;
		#endif

		palringoMessage *palMesg;
		palringoGroup 	*palGroup;
		palringoContact *palContact;

		string iniFile;
		string iniFile1;
		string iniFile2;
		string triviaIni;
		int patchStart;

		map<string, botMod> botMods;
		bool isMod(string userid);
		string searchMod(string modname);



		vector<string> botPests;
		bool isPest(string userid);

		vector<string> muteList;
		bool shouldMute(string userid);

		vector<string> banList;
		bool shouldBan(string userid);

		//Beta Features
		unsigned long   messagesReceived;
		unsigned long   messagesSent;
		bool            debug;
};

#endif // BASECLIENT_H
