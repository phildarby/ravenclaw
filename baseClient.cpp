/*
 * Software created by:
 * Michael Champagne
 * Raven (ID: 22885233 | 512388)

 * Licensed with GNU lpgl v3.0
 * Found in gnu-v3.txt or available online here
 * http://www.gnu.org/licenses/lgpl.txt
 */


#include "baseClient.h"

#include <boost/algorithm/string.hpp>

#include <stdlib.h>
#include <windows.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include <set>
#include <iomanip>
#include <mysql/mysql.h>

typedef std::vector<std::string> split_result_t;

using namespace std;
using std::right;
string timestamps()
{
    char buffer[256];

            //double days = (double)uptime/86400;

    time_t ltime; /* calendar time */
    ltime=time(NULL); /* get current cal time */
    snprintf(buffer, sizeof(buffer), "%s",asctime( localtime(&ltime) ) );
    return buffer;
}





const int MAX_RESP = 3;
const std::string delim = "?!.;,";

typedef vector<string> vstring;

vstring find_match( std::string input );
void copy( char *array[], vstring &v );
void preprocess_input( std::string &str );
void UpperCase( std::string &str );
void cleanString( std::string &str );
bool isPunc(char c);
int s = 0;

typedef struct {
	char *input;
	char *responses[MAX_RESP];
}record;

record KnowledgeBase[] = {
	{"WHAT IS YOUR NAME",
	{"MY NAME IS FAVZBOT V1.0.",
	 "YOU CAN CALL ME FAVZBOT.",
	 "WHY DO YOU WANT TO KNOW MY NAME?"}
	},

	{"HI",
	{"HI THERE!",
	 "HOW ARE YOU?",
	 "HI!"}
	},

	{"HOW ARE YOU",
	{"I'M DOING FINE!",
	"I'M DOING WELL AND YOU?",
	"WHY DO YOU WANT TO KNOW HOW AM I DOING?"}
	},

	{"WHO ARE YOU",
	{"I'M AN A.I PROGRAM.",
	 "I THINK THAT YOU KNOW WHO I'M.",
	 "WHY ARE YOU ASKING?"}
	},

	{"ARE YOU INTELLIGENT",
	{"YES,OFCORSE.",
	 "WHAT DO YOU THINK?",
	 "ACTUALY,I'M VERY INTELLIENT!"}
	},

	{"ARE YOU REAL",
	{"DOES THAT QUESTION REALLY MATERS TO YOU?",
	 "WHAT DO YOU MEAN BY THAT?",
	 "I'M AS REAL AS I CAN BE."}
	}
};
size_t nKnowledgeBaseSize = sizeof(KnowledgeBase)/sizeof(KnowledgeBase[0]);



string calculateTriviaScore()
{
    string output;
    map<string,int> names;
    ifstream nameList;
    nameList.open("triviascore.bin");
    string line;
    while(getline(nameList,line)){
        if ( ! line.empty() ) {
        ++names[line];
        }

    //while(!nameList.eof()){
        //getline(nameList, line);

    }


    map<string,int>::iterator it = names.begin();
    while (it != names.end()) {
        stringstream ss;//create a stringstream
        ss  << it->second;//add number to the stream
        stringstream ss1;//create a stringstream
        ss1  << left << setfill('.')<< setw(35)<< it->first;
        output.append(ss1.str()+"\t"+ss.str()+"\n");

        ++it;
    }
    return output;
}






baseClient::baseClient(map<string, string> botSettings)
{
    //Login Details | No more compile time username and password
	this->username 	= botSettings["username"];
	this->password 	= botSettings["password"];

	this->botAdmin 	= botSettings["botAdmin"];  //the ID of the user who owns the bot
	this->botName	= botSettings["botName"];   //Set the bots name to give it personality
	this->adminName	= botSettings["adminName"]; //used for printing the admins name in messages
	this->botId	    = botSettings["botId"];     //used for certain checks to make sure your not targeting the bot itself, ill eventually figure out how to avoid this
    this->botGroupName = botSettings["botGroupName"];
    this->botGroupID = botSettings["botGroupID"];
	//these set the way you call commands
	//ie #leave or /help || #rc leave or #rc help
	this->nameSpace = botSettings["nameSpace"];

	//question details
    this->triviaCategory 	= botSettings["triviaCategory"];
	this->question1 	= botSettings["question1"];
	this->question2 	= botSettings["question2"];
	this->question3 	= botSettings["question3"];
	this->question4 	= botSettings["question4"];
	this->question5 	= botSettings["question5"];
	this->question6 	= botSettings["question6"];
	this->question7 	= botSettings["question7"];
	this->question8 	= botSettings["question8"];
	this->question9 	= botSettings["question9"];
	this->question10 	= botSettings["question10"];
	this->question11 	= botSettings["question11"];
	this->question12 	= botSettings["question12"];
	this->question13 	= botSettings["question13"];
	this->question14 	= botSettings["question14"];
	this->question15 	= botSettings["question15"];
	this->question16 	= botSettings["question16"];
	this->question17 	= botSettings["question17"];
	this->question18 	= botSettings["question18"];
	this->question19 	= botSettings["question19"];
	this->question20 	= botSettings["question20"];
	this->question21 	= botSettings["question21"];
	this->question22 	= botSettings["question22"];
	this->question23 	= botSettings["question23"];
	this->question24 	= botSettings["question24"];
	this->question25 	= botSettings["question25"];
	this->question26 	= botSettings["question26"];
	this->question27 	= botSettings["question27"];
	this->question28 	= botSettings["question28"];
	this->question29 	= botSettings["question29"];
	this->question30 	= botSettings["question30"];
	this->question31 	= botSettings["question31"];
	this->question32 	= botSettings["question32"];
	this->question33 	= botSettings["question33"];
	this->question34 	= botSettings["question34"];
	this->question35 	= botSettings["question35"];
	this->question36 	= botSettings["question36"];
	this->question37 	= botSettings["question37"];
	this->question38 	= botSettings["question38"];
	this->question39 	= botSettings["question39"];
	this->question40 	= botSettings["question40"];
	this->question41 	= botSettings["question41"];
	this->question42 	= botSettings["question42"];
	this->question43 	= botSettings["question43"];
	this->question44 	= botSettings["question44"];
	this->question45 	= botSettings["question45"];
	this->question46 	= botSettings["question46"];
	this->question47 	= botSettings["question47"];
	this->question48 	= botSettings["question48"];
	this->question49 	= botSettings["question49"];
	this->question50 	= botSettings["question50"];
	this->answer1 	= botSettings["answer1"];
	this->answer2 	= botSettings["answer2"];
	this->answer3 	= botSettings["answer3"];
	this->answer4 	= botSettings["answer4"];
	this->answer5 	= botSettings["answer5"];
	this->answer6 	= botSettings["answer6"];
	this->answer7 	= botSettings["answer7"];
	this->answer8 	= botSettings["answer8"];
	this->answer9 	= botSettings["answer9"];
	this->answer10 	= botSettings["answer10"];
	this->answer11 	= botSettings["answer11"];
	this->answer12 	= botSettings["answer12"];
	this->answer13 	= botSettings["answer13"];
	this->answer14 	= botSettings["answer14"];
	this->answer15 	= botSettings["answer15"];
	this->answer16 	= botSettings["answer16"];
	this->answer17 	= botSettings["answer17"];
	this->answer18 	= botSettings["answer18"];
	this->answer19 	= botSettings["answer19"];
	this->answer20 	= botSettings["answer20"];
	this->answer21 	= botSettings["answer21"];
	this->answer22 	= botSettings["answer22"];
	this->answer23 	= botSettings["answer23"];
	this->answer24 	= botSettings["answer24"];
	this->answer25 	= botSettings["answer25"];
	this->answer26 	= botSettings["answer26"];
	this->answer27 	= botSettings["answer27"];
	this->answer28 	= botSettings["answer28"];
	this->answer29 	= botSettings["answer29"];
	this->answer30 	= botSettings["answer30"];
	this->answer31 	= botSettings["answer31"];
	this->answer32 	= botSettings["answer32"];
	this->answer33 	= botSettings["answer33"];
	this->answer34 	= botSettings["answer34"];
	this->answer35 	= botSettings["answer35"];
	this->answer36 	= botSettings["answer36"];
	this->answer37 	= botSettings["answer37"];
	this->answer38 	= botSettings["answer38"];
	this->answer39 	= botSettings["answer39"];
	this->answer40 	= botSettings["answer40"];
	this->answer41 	= botSettings["answer41"];
	this->answer42 	= botSettings["answer42"];
	this->answer43 	= botSettings["answer43"];
	this->answer44 	= botSettings["answer44"];
	this->answer45 	= botSettings["answer45"];
	this->answer46 	= botSettings["answer46"];
	this->answer47 	= botSettings["answer47"];
	this->answer48 	= botSettings["answer48"];
	this->answer49 	= botSettings["answer49"];
	this->answer50 	= botSettings["answer50"];

    //design the code so that we can either have or not have a namespace
    //IE: !rc test compared to !test
	if(this->nameSpace != "UNKNOWN")
    {
        this->cmdAdmin 	    = botSettings["cmdAdmin"] + this->nameSpace + " ";
        this->cmdBase	    = botSettings["cmdUser"]  + this->nameSpace + " ";
        this->patchStart    = 3;
        // !rc away message
        //  1   2      3
    }
    else
    {
		this->cmdAdmin 	    = botSettings["cmdAdmin"];
        this->cmdBase	    = botSettings["cmdUser"];
        this->patchStart 	= 2;
        // !away message
        //   1     2
    }

    //Feature is currently depreciated
	//controlGroup      = "[Group ID]"; //palringo
	//controlGroupName  = "[Group Name]"; //palringo

	//various "system" variables
	//Changed out canTalk works, it is no longer a global var to mute ALL sends
	//When the bot is muted i still want people to be able to use commands and such
	canTalk     = true;         //if the bot is muted or not
	adminOnline = false;        //if admin is online or not
	startTime   = time(NULL);   //sets the time the bot initializes
	startedTime = timestamps();
	security    = true;         //activates things like the group parser

	this->iniFile = botSettings["iniFile"];
	this->iniFile1 = botSettings["iniFile1"];
	this->iniFile2 = botSettings["iniFile2"];
	this->reloadIni();

	//Beta Features
	messagesReceived    = 0;
	messagesSent        = 0;
}



//triggered when a message is received
void baseClient::recv_groupMessage(string group, string user, string message)
{
    if(security && shouldMute(user))
    {
        this->admin_silence(group, user);

        this->send_message(group, "User <"+user+"> is a known pest");
        this->send_pm(botAdmin, "Attempted to Mute user <"+user+"> in group <"+group+">");

        if(botMods.size() > 0)
        {
            for(std::map<string, botMod>::iterator i = botMods.begin(); i != botMods.end(); ++i)
            {
                this->send_pm(i->first, "Attempted to Mute user <"+user+"> in group <"+group+">");
            }
        }
    }
    else if(security && shouldBan(user))
    {
        this->admin_ban(group, user);

        this->send_message(group, "User <"+user+"> is a known pest");
        this->send_pm(botAdmin, "Attempted to Ban user <"+user+"> in group <"+group+">");

        if(botMods.size() > 0)
        {
            for(std::map<string, botMod>::iterator i = botMods.begin(); i != botMods.end(); ++i)
            {
                this->send_pm(i->first, "Attempted to Ban user <"+user+"> in group <"+group+">");
            }
        }
    }
    else if(!isPest(user))
    {
        messagesReceived++;
        this->parse_groupMessage(group, user, engine.splitStr(message, ' '));
    }
}

//triggered when a PM is received
void baseClient::recv_personalMessage(string name, string user, string message)
{
    if(!isPest(user) and !shouldMute(user) and !shouldBan(user))
    {
        this->parse_personalMessage(name, user, engine.splitStr(message,' '));

		#ifdef RAVENCLAW_DEBUG
            palDB.userRegister(user, name);
 		#endif
    }
}

//triggered when anyone joins or leaves a group
void baseClient::group_update(map<string, string> updatePacket)
{
    string group = updatePacket["Group-Id"];
    string groupName = updatePacket["Group-Name"];

    if(updatePacket["Contact-Id"] != this->botId)
    {
        if(updatePacket["Type"] == "0")
        {
            string nickname = updatePacket["Nickname"];
            string userid = updatePacket["Contact-Id"];
            if(security && shouldMute(userid))
            {
                this->admin_silence(group, userid);
                this->send_message(group, "User "+nickname+" is a known pest");
            }
            else if(security && shouldBan(userid))
            {
                this->admin_ban(group, userid);
                this->send_message(group, "User "+nickname+" is a known pest");
            }
            else
            {
                string temp;
                this->send_message(group, "/me "+nickname+" joined " +palDB.groupLookUp(group) );
                this->send_pm(botAdmin, nickname+" joined " +palDB.groupLookUp(group));


                if(botMods.size() > 0)
                    {
                        //boost::thread t(pmModJoin(group,nickname));
                        //t.join();

                        for(std::map<string, botMod>::iterator i = botMods.begin(); i != botMods.end(); ++i)
                        {
                            //botMod temp = botMods.find()->first;
                            this->send_pm(i->first, nickname+" joined " +palDB.groupLookUp(group));
                            Sleep(100);

                        }
                   }


            }

			#ifdef RAVENCLAW_DEBUG
            palDB.userRegister(userid, nickname);
			#endif
        }
        else
        {


               // string nickname = updatePacket["Nickname"];
                //string userid = updatePacket["Contact-Id"];
                string buffer = palDB.userLookUp(updatePacket["Contact-Id"]);


                this->send_message(group, buffer + " left "+palDB.groupLookUp(group));

        }
    }
}

//triggered when anyone commits an admin action
void baseClient::group_admin(string group, string admin, string user, string action)
{
    if(user != botId)
    {
        if(action == ACTION_ADMIN)
            this->send_message(group, "Congrats!\r\nJust dont power abuse");
        else if(action == ACTION_MOD)
            this->send_message(group, "Aww thats cute\r\nYou think you have power");
        else if(action == ACTION_SILENCE)
            this->send_message(group, "I love duct tape!");
        else if(action == ACTION_RESET)
            this->send_message(group, "HaHaHa they reset you :P");
        else if(action == ACTION_KICK)
            this->send_message(group, "Get told sucka");
        else if(action == ACTION_BAN)
            this->send_message(group, "Uh oh here comes the buthurt");
    }
    else
    {
        if(action == ACTION_ADMIN)
            this->send_message(group, "Thank you very much :D");
        else if(action == ACTION_MOD)
            this->send_message(group, "Woot I'm cool now");
    }

	#ifdef RAVENCLAW_DEBUG
    palDB.logAdminAction(group, admin, user, action);
	#endif
}

//ive left in some of the basic bot features im writting for my personal bot to give you a feel of how i do things




void baseClient::parse_groupMessage(string group, string user, vector<string> data)
{
    //Will be used to print the map later.
    map<string, int> scores;
    string trivianames;
    int triviascores;




	int     blocks  = data.size();
	string  mesg    = this->messagePatcher(data, " ", 1);


	#ifdef RAVENCLAW_DEBUG
	//palDB.logChat(group, user, mesg);
	#endif

    //new method of creating a command allows us to create "namespace commands" that palringo requires
    //IE: `#rc help` rather than `#help`
    string cmd;
    if(blocks >= patchStart-1)
    {
        if(this->nameSpace != "UNKNOWN")
        {
            cmd = data[0] + " ";

            if(cmd == cmdAdmin || cmd == cmdBase)
            {
               cmd = data[0] + " "+ data[1];
            }
            else
            {
                cmd = data[0];
            }
        }
        else
        {
            cmd = data[0];
        }
    }
    else
    {
        cmd = data[0];
    }

    //users online and bot security
    {
        //signs admin back online
        if(user == botAdmin && adminOnline == false)
        {
            //sets the bot admins status to being back online
            adminOnline = true;
            adminMessage = "";
            //this->send_message(group, adminName+" is back online!");
        }

        //signs mods back online
        if(isMod(user))
        {
            if(botMods[user].name != "" and botMods[user].online == false)
            {
                botMods[user].online = true;
                botMods[user].message = "";
                //this->send_message(group, botMods[user].name+" is back online!");
            }
        }

        //little security features like muting people who post group links
        if(security == true && user != botAdmin && !isMod(user))
        {
            //checks if someone posted a group link
            if(mesg.find("[") != std::string::npos && mesg.find("]") != std::string::npos)
            {
                this->send_message(group, "Don't post group links without permission!");

                //buffer to save string to
                char const* buff = mesg.c_str();
                int length = strlen(buff);

                string groupName;
                int sPos = 0;
                int ePos = 0;

                //loop through the string and find the location of brackets
                for(int i=0; i < length; i++)
                {
                    if(buff[i] == '[')
                        sPos = i+1;

                    if(buff[i] == ']')
                        ePos = i;

                    if(sPos != 0 and ePos != 0)
                        break;
                }

                //if both brackets exit do some things
                if(ePos != 0)
                {
                    groupName = mesg.substr(sPos, ePos-sPos);
                    //this->admin_ban(group, user);
                    //this->send_message(group, "Don't make me come into `"+groupName+"` and spam it!");
                }
            }
        }
    }

    //admin commands
    if(user == botAdmin)
    {
        if(cmd == cmdAdmin+"check")
        {
            if(canTalk == true)
                this->send_message(group, "I am online!");
            else
            {
                //if the bots been muted this will still run
                canTalk = true;
                this->send_message(group, "I've been muted but im here ;_;");
                canTalk = false;
            }
        }
        else if(cmd == cmdAdmin+"leave")
        {
            this->group_part(group);
        }
        else if(cmd == cmdAdmin+"join")
        {
            if(blocks >= patchStart)
            {
                if(mesg.find("[") != std::string::npos && mesg.find("]") != std::string::npos)
                {
                    //buffer to save string to
                    char const* buffer = mesg.c_str();
                    int length = strlen(buffer);

                    string groupName;
                    string password;
                    int sPos = 0;
                    int ePos = 0;

                    //loop through the string and find the location of brackets
                    for(int i=0; i < length; i++)
                    {
                        if(buffer[i] == '[')
                            sPos = i+1;

                        if(buffer[i] == ']')
                            ePos = i;

                        if(ePos != 0)
                            break;
                    }

                    //if both brackets exit do some things
                    if(ePos != 0)
                    {
                        //set group name
                        groupName = mesg.substr(sPos, ePos-sPos);

                        //find password
                        if((sPos != 0 and ePos != 0) and (buffer[ePos+1] == ' '))
                            password = mesg.substr(ePos+2, length);
                        else
                            password = "";

                        //join the group
                        this->send_message(group, "Joining: "+groupName);
                        this->group_join(groupName, password);

                    }
                }
                else
                {
                    string groupName = this->messagePatcher(data, " ", patchStart);

                    this->send_message(group, "Joining: "+groupName);
                    this->group_join(groupName);
                }
            }
            else
            {
                this->send_message(group, cmdAdmin+"join [<group name>] <password>");
            }
        }
        else if(cmd == cmdAdmin+"msg")
        {
            if(blocks > patchStart)
            {
                //generate message
                string buff = this->messagePatcher(data, " ", patchStart+1);
                string user = data[patchStart-1];

                //send the buffer
                this->send_pm(user, buff);
                this->send_message(group, "Message: "+buff+"\r\nUser: "+user);
            }
            else
            {
                this->send_message(group, cmdAdmin+"msg <user ID> <message>");
            }
        }
        else if(cmd == cmdAdmin+"kick")
        {
            if(blocks > patchStart-1)
            {
                //generate message

                string user = this->messagePatcher(data, " ", patchStart);

                //send the buffer

                this->admin_kick(botGroupID, user);

            }

        }
        else if(cmd == cmdAdmin+"away")
        {
            /* Allows the bot admin to go afk, and the bot will know your not online, and leaves a message showing your not online */
            if(blocks > patchStart-1)
            {
                adminMessage	= this->messagePatcher(data, " ", patchStart);
                adminOnline		= false;
                this->send_message(group, "Bye " + adminName + "!\r\nAnd don't worry I saved the message\r\n"+adminMessage);
            }
            else
            {
                adminOnline = false;
                this->send_message(group, "Bye "+adminName+"!");
            }

        }
        else if(cmd == cmdAdmin+"mute")
        {
            if(canTalk == true)
            {
                this->send_message(group, "Muted myself");
                canTalk = false;
            }
            else
            {
                canTalk = true;
                this->send_message(group, "I can talk again!");
            }
        }
        else if(cmd == cmdAdmin+"secure")
        {
            if(security == true)
            {
                this->send_message(group, "Turning off security systems!");
                security = false;
            }
            else
            {
                security = true;
                this->send_message(group, "Turning on security systems");
            }
        }
        /*else if(cmd == cmdAdmin+"test")
        {
            this->send_message(group, "Sending developer packet");
            this->send_debug(group);

            this->send_message(group, "Sending Transparency Test Image");
            this->send_image(group, "covertlogo.png");
            //engine.getUrl("http://google.com");
            //curl.getUrl("http://google.com");

            string buffer = Curl.postUrl("http://127.0.0.1/palringo/postData.html","d=testdata");
            this->send_message(group, buffer);
        }*/
        else if(cmd == cmdAdmin+"reload")
        {
            this->reloadIni(group);
        }
        else if(cmd == cmdAdmin+"info")
        {
            string buffer;
            string secure = "False";
            if(security == true)
            {
                secure = "True";
            }

            string mute = "False";
            if(this->canTalk != true)
            {
                mute = "True";
            }

            double uptime = difftime(time(NULL), startTime);

            char uptimeBuffer[256];
            int days    = uptime / 86400;
            int hours   = (uptime - 86400 * days) / 3600;
            int minutes = ((uptime - 86400 * days) - (3600 * hours))/60;
            int seconds = ((uptime - 86400 * days) - (3600 * hours))- 60 * minutes;

            //double days = (double)uptime/86400;
            snprintf(uptimeBuffer, sizeof(buffer), "%id %ih %im %is", days, hours, minutes, seconds);

            buffer +=   "Bot Information\r\n\r\n"
                        "Bot Admin: "+adminName+
                        "\r\nBot Name: "+botName+
                        "\r\nMods: "+engine.i2s(botMods.size())+
                        "\r\nSecurity Enabled: "+secure+
                        "\r\nBot Muted: "+mute+

                        "\r\nPests: "+engine.i2s(botPests.size())+
                        "\r\nAuto Ban: "+engine.i2s(banList.size())+
                        "\r\nAuto Mute: "+engine.i2s(muteList.size())+

                        "\r\nUptime: "+startedTime+"("+uptimeBuffer+")"+
                        "\r\nMessages Received: "+engine.l2s(messagesReceived)+
                        "\r\nMessages Sent: "+engine.l2s(messagesSent);

            this->send_message(group, buffer);

        }
		#ifdef RAVENCLAW_DEBUG
        else if(cmd == cmdAdmin+"register")
		{
		    if(blocks >= patchStart)
            {
                string groupName = this->messagePatcher(data, " ", patchStart);
                palDB.groupRegister(group, groupName);
                this->send_message(group, "You have registered this group with the name <"+groupName+">");

            }









                //map<string, string> dbLookup = palDB.groupRegister(group, groupName);

                //if(dbLookup["success"] == "true")
               // {
                 //   this->send_message(group, "You have registered this group with the name <"+groupName+">");
               // }
               // else
               // {
                //    this->send_message(group, "There was an error registering the group");
                //}
            //}
            //else
           // {
             //   this->send_message(group, "Sorry thats not how you use this command\r\n"+cmdAdmin+"register <groupname>");
            }

        else if(cmd == cmdAdmin+"group")
		{

                string groupName = palDB.groupLookUp(group);

                this->send_message(group, "The group is currently registered under "+groupName);

		}
		#endif
        else if(cmd == cmdAdmin+"help")
        {
             this->send_message(group, 	"Admin Help\r\n"+
                                        cmdAdmin+"check\r\n"+
                                        cmdAdmin+"leave\r\n"+
                                        cmdAdmin+"join [<group name>] <password>\r\n"+
                                        cmdAdmin+"msg <user id> <message>\r\n"+
                                        cmdAdmin+"away <message>\r\n"+
                                        cmdAdmin+"secure (also unsecure)\r\n"+
                                        cmdAdmin+"mute (also unmute)"+
                                        cmdAdmin+"test\r\n"+
                                        cmdAdmin+"reload\r\n"+
                                        cmdAdmin+"info"
										//#ifdef RAVENCLAW_DEBUG
										"\r\n"+cmdAdmin+"register <groupname>\r\n"+
                                        cmdAdmin+"group"
										//#endif
                                        );
        }
        else if(cmd == cmdAdmin+"registermod")
        {
            if(blocks > patchStart-1)
            {

                //string modNumbersInput = reader.Get("MODS", "modAccounts", "null");
                //string modNamesInput = reader.Get("MODS", "modNames", "null");
                const std::string useridName = this->messagePatcher(data, "\t", patchStart);
                split_result_t result;
                boost::split(result, useridName, boost::is_any_of("\t "));



                string modNumbersInput = result.at(0);
                string modNamesInput = result.at(1);

                //this->send_message(group, modNumbersInput);
                //this->send_message(group, modNamesInput);


                palDB.modRegister(modNumbersInput, modNamesInput);
                this->send_message(group, "You have successfully registered" +modNamesInput);


            }
        }
    }

    //mod commands
    if(isMod(user))
    {
        if(cmd == cmdAdmin+"check")
        {
            if(canTalk == true)
                this->send_message(group, "I am online!");
            else
            {
                //if shes been muted this will run
                canTalk = true;
                this->send_message(group, "I've been muted but im here ;_;");
                canTalk = false;
            }
        }
        else if(cmd == cmdAdmin+"leave")
        {
            this->group_part(group);
        }
        else if(cmd == cmdAdmin+"join")
        {
            if(blocks >= patchStart)
            {
                if(mesg.find("[") != std::string::npos && mesg.find("]") != std::string::npos)
                {
                    //buffer to save string to
                    char const* buffer = mesg.c_str();
                    int length = strlen(buffer);

                    string groupName;
                    string password;
                    int sPos = 0;
                    int ePos = 0;

                    //loop through the string and find the location of brackets
                    for(int i=0; i < length; i++)
                    {
                        if(buffer[i] == '[')
                            sPos = i+1;

                        if(buffer[i] == ']')
                            ePos = i;

                        if(ePos != 0)
                            break;
                    }

                    //if both brackets exit do some things
                    if(ePos != 0)
                    {
                        //set group name
                        groupName = mesg.substr(sPos, ePos-sPos);

                        //find password
                        if((sPos != 0 and ePos != 0) and (buffer[ePos+1] == ' '))
                            password = mesg.substr(ePos+2, length);
                        else
                            password = "";

                        //join the group
                        this->send_message(group, "Joining: "+groupName);
                        this->group_join(groupName, password);

                    }
                }
                else
                {
                    string groupName = this->messagePatcher(data, " ", patchStart);

                    this->send_message(group, "Joining: "+groupName);
                    this->group_join(groupName);
                }
            }
            else
            {
                this->send_message(group, cmdAdmin+"join [<group name>] <password>");
            }
        }
        else if(cmd == cmdAdmin+"msg")
        {
            if(blocks > patchStart)
            {
                //generate message
                string buff = this->messagePatcher(data, " ", patchStart+1);
                string user = data[patchStart-1];

                //send the buffer
                this->send_pm(user, buff);
                this->send_message(group, "Message: "+buff+"\r\nUser: "+user);
            }
            else
            {
                this->send_message(group, cmdAdmin+"msg <user ID> <message>");
            }
        }
        else if(cmd == cmdAdmin+"kick")
        {
            if(blocks > patchStart-1)
            {
                //generate message

                string user = this->messagePatcher(data, " ", patchStart);

                //send the buffer

                this->admin_kick(botGroupID, user);

            }

        }
        else if(cmd == cmdAdmin+"away")
        {
            /* Allows the bot admin to go afk, and the bot will know your not online, and leaves a message showing your not online */
            if(blocks > patchStart-1)
            {
				botMods[user].message	= this->messagePatcher(data, " ", patchStart);
				botMods[user].online	= false;
				this->send_message(group, "Bye " + botMods[user].name + "!\r\nAnd don't worry I saved the message");
            }
            else
            {
				botMods[user].online = false;
				this->send_message(group, "Bye "+botMods[user].name+"!");
            }

        }
        else if(cmd == cmdAdmin+"mute")
        {
            if(canTalk == true)
            {
                this->send_message(group, "Muted myself");
                canTalk = false;
            }
            else
            {
                canTalk = true;
                this->send_message(group, "I can talk again!");
            }
        }

        else if(cmd == cmdAdmin+"info")
        {
            string buffer;
            string secure = "False";
            if(security == true)
            {
                secure = "True";
            }

            string mute = "False";
            if(this->canTalk == true)
            {
                mute = "True";
            }

            double uptime = difftime(time(NULL), startTime);

            char uptimeBuffer[256];
            int days    = uptime / 86400;
            int hours   = (uptime - 86400 * days) / 3600;
            int minutes = ((uptime - 86400 * days) - (3600 * hours))/60;
            int seconds = ((uptime - 86400 * days) - (3600 * hours))- 60 * minutes;

            //double days = (double)uptime/86400;
            snprintf(uptimeBuffer, sizeof(buffer), "Uptime: %id %ih %im %is", days, hours, minutes, seconds);

            buffer +=   "Bot Information\r\n\r\n"
                        "Bot Admin: "+adminName+
                        "\r\nBot Name: "+botName+
                        "\r\nMods: "+engine.i2s(botMods.size())+
                        "\r\nSecurity Enabled: "+secure+
                        "\r\nBot Muted: "+mute+

                        "\r\nPests: "+engine.i2s(botPests.size())+
                        "\r\nAuto Ban: "+engine.i2s(banList.size())+
                        "\r\nAuto Mute: "+engine.i2s(muteList.size())+

                        "\r\nUptime: "+startedTime+"("+uptimeBuffer+")"+
                        "\r\nMessages Received: "+engine.l2s(messagesReceived)+
                        "\r\nMessages Sent: "+engine.l2s(messagesSent);

            this->send_message(group, buffer);

        }



        else if(cmd == cmdAdmin+"help")
        {
             this->send_message(group, 	"Mod Help\r\n"+
                                        cmdAdmin+"check\r\n"+
                                        cmdAdmin+"leave\r\n"+
                                        cmdAdmin+"join [<group name>] <password>\r\n"+
                                        cmdAdmin+"msg <user id> <message>\r\n"+
                                        cmdAdmin+"away <message>\r\n"+
                                        cmdAdmin+"mute (also unmute)"+
                                        cmdAdmin+"info"
                                        );
        }
        else if(cmd == cmdAdmin+"registermod")
        {
            if(blocks > patchStart-1)
            {

                //string modNumbersInput = reader.Get("MODS", "modAccounts", "null");
                //string modNamesInput = reader.Get("MODS", "modNames", "null");
                const std::string useridName = this->messagePatcher(data, "\t", patchStart);
                split_result_t result;
                boost::split(result, useridName, boost::is_any_of("\t "));



                string modNumbersInput = result.at(0);
                string modNamesInput = result.at(1);

                //this->send_message(group, modNumbersInput);
                //this->send_message(group, modNamesInput);


                palDB.modRegister(modNumbersInput, modNamesInput);
                this->send_message(group, "You have successfully registered" +modNamesInput);


            }
        }
    }

    //user commands
    {
        //TODO: encode various text data for URL queries
        if(cmd == cmdBase+"admin")
        {
            string output = "The bot owner is "+adminName+" Userid: "+botAdmin;

            if(adminOnline)
                output += "\r\nAnd they are online";
            else
            {
                 output += "\r\nAnd they are offline";

                 if(adminMessage != "")
                    output += "\r\nBut they left a message!\r\n"+adminMessage;
            }

            this->send_message(group, output);
        }

        else if(cmd == cmdBase+"help")
        {
            this->send_message(group,   "User Help\r\n"
                                        "< and > signs are used to show you command arguments, do not type them out!\r\n"+
                                        //cmdBase+"admin\r\n"+
                                        cmdBase+"trivia <Starts trivia>\r\n"+
                                        cmdBase+"quit(Quits trivia game)\r\n"+
                                        cmdBase+"register <query> (adds yourself to bot)\r\n"+
                                        cmdBase+"nickname (shows your registered nickname with bot)\r\n"+
                                        cmdBase+"triviascore (Shows scores)\r\n"+
                                        cmdBase+"google <query>\r\n"+
                                        cmdBase+"youtube <query>\r\n"+
                                        cmdBase+"uptime\r\n"+
                                        cmdBase+"dice <coin,6,8,10,12 or 20>\r\n"+
                                        cmdBase+"credits\r\n");
                                        //cmdBase+"call <calls for an admin/mod>\r\n"
                                                                              //  #ifdef RAVENCLAW_DEBUG
                                        /*"\r\n"+cmdBase+"nickname 'Shows your current registered nickname'\r\n"+
                                        cmdBase+"register <nickname>"*/
                                                                               // #endif

        }
        else if(cmd == cmdBase+"credits")
        {
            this->send_message(group,   " bot created by Dem \r\n\r\n Thanks to Rave'n for creating base of this bot I would not of been able to have created this without his help");
        }
        else if(cmd == cmdBase+"website")
        {
            this->send_message(group, "http://www.palringo.com/groups/invite/4109857");
        }

        else if(cmd == cmdBase+"cache")
        {
            this->send_message(group, "palringo://debug/reset/cache");
        }

        else if(cmd == cmdBase+"quote")
        {
            srand( time(0) ); //seed random number generator
            string returnmsg[8] = { "SMILE, the most precious humanly expression! Perhaps, a smile could be attached with tremendous scopes than any other humanly expressions. It might avoid a confrontation, a fight, a battle or even a war. How hard, cruel or mighty a person may be, a naive smile could bring him to his knees. -Chandrababu V.S.",
                                    "Instead of criticizing use the time improve yourself and the things around you -Unknown",
                                    "Writing and reading love poetry is a way to get in touch with your inner feelings about the emotions of loving and being loved - Maanda stanley",
                                    "When life gets tough, the tough get tougher, and the rough get rougher. No matter how rough and tough our life becomes we're rougher and tougher. -TTM",
                                    "I will never be ashamed or have too much pride in telling someone that I miss them, because one day, I may never get another chance.-Unknown",
                                    "Do not judge others by their looks and social status.Who knows what an amazing person he/she may be.- Ssk",
                                    "Be yourself no matter what, if they don't like it, then don't waste your time cause they aint worth it.",
                                    "Wisdom is knowing fully, understanding completely and accepting really in life the things and thoughts that help us to be truly honest to ourselves sustainably"};
            int randomName = rand() %8; //return a number from [0,3)
            this->send_message(group, returnmsg[randomName]);
        }
        else if(cmd == cmdBase+"trivia")
        {
            this->send_message(group, "Welcome to trivia! \n\nEveryone please register your name with bot using "+
                               cmdBase+" register <your nickname> without <>! \n\nTo see if your registered type "+cmdBase+" nickname\n\n"
                               "To see scores at any time type "+cmdBase+" triviascore\n\n Once everyone registers please type start trivia");



            s = -1;
        }

        /*else if(cmd == cmdBase+"addtrivia")
        {
            if(blocks > patchStart-1)
            {
                string playerNumbersInput = user;
                string playerNamesInput = this->messagePatcher(data, " ", patchStart);
                vector<string> playerNumbers;
                vector<string> playerNames;
                botPlayer temp;
                temp.id = playerNumbersInput;
                temp.name = playerNamesInput;

                this->botPlayers[playerNumbersInput] = temp;
                this->send_message(group, "Thank you, you have been added to the bot!");
                string playerID = user + "|" ;
                string playerName = this->messagePatcher(data, "", patchStart) + "|";
                ofstream triviaPlayersID;
                triviaPlayersID.open ("triviaplayersId.bin", ios::out | ios::app | ios::binary);
                triviaPlayersID << playerID ;
                triviaPlayersID.close();
                ofstream triviaPlayersName;
                triviaPlayersName.open ("triviaplayersName.bin", ios::out | ios::app | ios::binary);
                triviaPlayersName << playerName ;
                triviaPlayersName.close();


            }

        }*/
        else if(cmd == cmdBase+"quit")
        {
            this->send_message(group, "Time must be up Thanks for playing!");
            s = 0;

        }
        else if(cmd == cmdBase+"triviascore")
        {
            this->send_message(group, "Final Score Totals: \n"+calculateTriviaScore());


        }
        else if(cmd == cmdBase+"google")
        {
            if(blocks > patchStart-1)
            {
                string search = "Here you go\r\nhttp://www.google.com/search?q=";
                search.append(this->messagePatcher(data, "+", patchStart));
                this->send_message(group, search);
            }
            else
            {
                this->send_message(group, "Sorry thats not how you use this command\r\n"+cmdBase+"google <search query>");
            }
        }
        else if(cmd == cmdBase+"youtube")
        {
            if(blocks > patchStart-1)
            {
                string search = "Videos are fun!\r\nhttp://www.youtube.com/results?search_query=";
                search.append(this->messagePatcher(data, "+", patchStart));
                this->send_message(group, search);
            }
            else
            {
                this->send_message(group, "Sorry hun thats not how you use this command\r\n"+cmdBase+"youtube <search query>");
            }
        }
        else if(cmd == cmdBase+"dice")
        {
            if(blocks == patchStart)
            {
                srand (time(NULL));
                int dieRoll = 0;
                string coin = "";
                string diceType = data[patchStart-1];


                if(diceType == "coin")
                {
                    dieRoll = rand() % 50 + 1;

                    if(dieRoll < 25)
                        coin = "heads";
                    else
                        coin = "tails";
                }
                else if(diceType == "6" || diceType == "d6")
                    dieRoll = rand() % 6 + 1;
                else if(diceType == "8" || diceType == "d8")
                    dieRoll = rand() % 8 + 1;
                else if(diceType == "10" || diceType == "d10")
                    dieRoll = rand() % 10 + 1;
                else if(diceType == "12" || diceType == "d12")
                    dieRoll = rand() % 12 + 1;
                else if(diceType == "20" || diceType == "d20")
                    dieRoll = rand() % 20 + 1;

                if(dieRoll != 0 && coin == "")
                {
                    this->send_message(group, "And the DM rolls a "+engine.i2s(dieRoll));
                }
                else if(coin != "")
                {
                    this->send_message(group, "And its "+coin);
                }
                else
                {
                    this->send_message(group, "Not a valid dice type");
                }
            }
            else
            {
                this->send_message(group, "Sorry thats not how you use this command\r\n"+cmdBase+"dice <coin,6,8,10,12 or 20>");
            }
        }



        else if(cmd == cmdBase+"mod" or cmd == cmdBase+"mods")
        {

            if(botMods.size() > 0)
            {

                string output = "The bot now has mods\r\n";

                for(std::map<string, botMod>::iterator i = botMods.begin(); i != botMods.end(); ++i)
                {
                    output.append("\r\n"+i->second.name);


                }

                this->send_message(group, output);
            }
            else
                this->send_message(group, "The bot currently has no mods assigned");
        }
        /*else if(cmd == cmdBase+"player" or cmd == cmdBase+"players")
        {

            if(botPlayers.size() > 0)
            {
                string output = "The bot now has trivia players\r\n";

                for(std::map<string, botPlayer>::iterator i = botPlayers.begin(); i != botPlayers.end(); ++i)
                {
                    output.append("\r\n"+i->second.name);


                }

                this->send_message(group, output);
            }
            else
                this->send_message(group, "The bot currently has no players assigned");
        }*/

        else if(cmd == cmdBase+"uptime")
        {
            string buffer;
            //string buffer1;
            double uptime = difftime(time(NULL), startTime);

            char uptimeBuffer[256];
            int days    = uptime / 86400;
            int hours   = (uptime - 86400 * days) / 3600;
            int minutes = ((uptime - 86400 * days) - (3600 * hours))/60;
            int seconds = ((uptime - 86400 * days) - (3600 * hours))- 60 * minutes;

            //double days = (double)uptime/86400;
            snprintf(uptimeBuffer, sizeof(uptimeBuffer), "Uptime: %id %ih %im %is", days, hours, minutes, seconds);

            buffer.append("Bot UpTime\r\n\r\n");
            buffer.append(uptimeBuffer);
            buffer.append("\r\nBot Started: "+startedTime);




            this->send_message(group, buffer);
        }
        else if(cmd == cmdBase+"call")
        {
            if(botMods.size() > 0)
                    {

                        for(std::map<string, botMod>::iterator i = botMods.begin(); i != botMods.end(); ++i)
                        {
                            //botMod temp = botMods.find()->first;
                            this->send_pm(i->first, " Someone is looking for an admin/mod in " +botGroupName);

                        }
                    }

        }

        else if(cmd == cmdBase+"nickname")
        {
            //contact buffer = palContact->client_lookup(user);




            /*if(isPlayer(user))
            {

                this->send_message(group, "you are currently registered under "+botPlayers[user].name);
                //this->send_message(group, "Your palringo name is " + buffer.nickname);
            }*/
            if(palDB.userLookUp(user)!= "")
            {
                this->send_message(group, "you are currently registered under "+palDB.userLookUp(user));

            }
            else
            {
                this->send_message(group, "you are not currently registered to the bot\r\nPlease run the command "+cmdBase+"register <user name>");
            }
//            this->send_message(group, "you are currently registered under "+buffer);
        }

        else if(cmd == cmdBase+"importusers")
        {
            if(botMods.size() > 0)
            {
                string userid;
                string usernames;

                for(std::map<string, botMod>::iterator i = botMods.begin(); i != botMods.end(); ++i)
                {
                    userid = i->first;
                    usernames = i->second.name;

                    palDB.modRegister(userid,usernames);
                }
            }
        }

        else if(cmd == cmdBase+"register")
        {
            if(blocks > patchStart-1)
            {



                string playerNumbersInput = user;
                string playerNamesInput = this->messagePatcher(data, " ", patchStart);
                palDB.userRegister(playerNumbersInput, playerNamesInput);
                this->send_message(group, "Thank you, you have been added to the bot!");



                /*vector<string> playerNumbers;
                vector<string> playerNames;
                botPlayer temp;
                temp.id = playerNumbersInput;
                temp.name = playerNamesInput;

                this->botPlayers[playerNumbersInput] = temp;
                this->send_message(group, "Thank you, you have been added to the bot!");
                string playerID = user + "|" ;
                string playerName = this->messagePatcher(data, "", patchStart) + "|";
                ofstream triviaPlayersID;
                triviaPlayersID.open ("triviaplayersId.bin", ios::out | ios::app | ios::binary);
                triviaPlayersID << playerID ;
                triviaPlayersID.close();
                ofstream triviaPlayersName;
                triviaPlayersName.open ("triviaplayersName.bin", ios::out | ios::app | ios::binary);
                triviaPlayersName << playerName ;
                triviaPlayersName.close();*/


            }


            else
            {
                this->send_message(group, "Sorry thats not how you use this command\r\n"+cmdBase+"register <nickname>");
            }
        }

    }

    //Bot AI
    if(cmd == botName && canTalk) /* "talk to the bot" */
        {
                if(blocks >= 2)
                {
                        string aiCommand = this->messagePatcher(data, " ", 2);

                        //command phrases
                        //such basic AI haha
                        if(aiCommand == "is funny")
                        {
                                if(user == botAdmin)
                                {
                                        this->send_message(group, "Of course I am");
                                }
                                else
                                {
                                        this->send_message(group, "im not just funny im amazing :3");
                                }
                        }
                        else if(aiCommand == "how are you" || aiCommand == "how are you?")
            {
                if(user == botAdmin)
                                {
                                        this->send_message(group, "You know im doing good, but thanks for asking");
                                }
                                else
                                {
                                        this->send_message(group, "I'm good thanks for asking!");
                                }
            }
                        else if(aiCommand == "is cute")
                        {
                                if(user == botAdmin)
                                {
                                        this->send_message(group, "Thank you "+botAdmin+" :3");
                                }
                                else
                                {
                                        this->send_message(group, "i know im cute");
                                        this->send_message(group, "im flattered\r\nbut seriously\r\nim just some computer code");
                                        this->send_message(group, "#android-problems");
                                }
                        }
                        else if(aiCommand == "what is your favorite movie")
                        {
                                if(user == botAdmin)
                                {
                                        this->send_message(group, "I can't tell you "+botAdmin+" :3");
                                }
                                else
                                {
                                        this->send_message(group, "the matrix of course");
                                        this->send_message(group, "take the blue pill ");
                                        this->send_message(group, "folow the white rabbit");
                                }
                        }
                        else if(aiCommand == "is ugly")
                        {
                                if(user == botAdmin)
                                {
                                        this->send_message(group, "Stop being a bish "+botAdmin);
                                }
                                else
                                {
                                        this->send_message(group, "/me cries");
                                        this->send_message(group, "You're a bish >:C");
                                }
                        }
                }
                else
                {
                        this->send_message(group, "Hello :3");
                }
        }

        //Word filters and other shit

        else if(canTalk)
        {

                if(blocks >= 1) /* phrase filters */
                {

                        //generates our phrase with our magical messagePatcher <3
                        string phrase = this->messagePatcher(data, " ", 1);




                        if(phrase == "bye bot")
                        {
                            srand( time(0) ); //seed random number generator
                            string returnmsg[3] = { "see ya soon :D", "Wait.... Where you think your going", "Dont let the door hit ya on way out"};
                            int randomName = rand() %3; //return a number from [0,3)
                            this->send_message(group, returnmsg[randomName]);

                        }

                        else if(phrase == "what is your name")
                        {



                            srand( time(0) ); //seed random number generator
                            string returnmsg[3] = { "MY NAME IS " +botName, "YOU CAN CALL ME "+botName, "WHY DO YOU WANT TO KNOW MY NAME?"};
                            int randomName = rand() %3; //return a number from [0,3)
                            this->send_message(group, returnmsg[randomName]);
                        }
                        else if (phrase == "hi bot")
                        {
                            if (palDB.userLookUp(user)!= "")
                            {
                                this->send_message(group, "Hello "+palDB.userLookUp(user));
                            }
                            else
                            this->send_message(group, "Hello :)");



                        }


                        else if(phrase == "bye guys")
                        {
                            if (palDB.userLookUp(user)!= "")
                            {
                                this->send_message(group, "/me waves goodbye to "+palDB.userLookUp(user));
                            }
                            else

                                    this->send_message(group, "/me waves goodbye ");

                        }

                        else if(phrase == "hi dem")
                        {
                            if (palDB.userLookUp(user)!= "")
                            {
                                this->send_message(group, palDB.userLookUp(user) +" (l) dem :P");
                            }
                            else

                                    this->send_message(group, "someone (l) dem :P");


                        }
                        //kh
                        else if(phrase == "who is awesome")
                        {
                            srand( time(0) ); //seed random number generator
                            string returnmsg[5] = { "/me Luci is awesome", "/me Sarah is awesome", "/me Lass is awesome", "/me Spud is awesome", "/me Rachel is awesome"};
                            int randomName = rand() %5; //return a number from [0,5)
                            //this->send_message(group, returnmsg[randomName]);
                            if (palDB.userLookUp(user)!= "")
                            {
                                this->send_message(group, palDB.userLookUp(user)+ " is awesome");
                            }
                            else

                            this->send_message(group, "you are awesome");


                        }
                        else if(phrase == "start trivia" && s == -1)
                        {
                            ofstream triviaPlayers;
                            triviaPlayers.open ("triviascore.bin", ios::out);
                            triviaPlayers << "\0" ;
                            triviaPlayers.close();

                            this->send_message(group, "Rules for trivia:\n1. Bot will ask questions for 1 hour\n2. First to guess right is awarded 1 point\n3. Full answer required for point no typo or abreviation\n4. No google or other search engines\n"
                               "5. Bot records first correct answer and has final say\n6. Only 5 guesses per minute per member\n7. The winner of tonights trivia will get to choose next set of questions and give hints if needed during that time\n"
                               "8. All winners will be tested for PED's\n\n");
                            Sleep(10000);

                            this->send_message(group, triviaCategory);
                            Sleep(5000);
                            this->send_message(group, question1);
                            s = 1;







                        }
                        else if (phrase == answer1 && s == 1){



                                     this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);
                                      scores[user] = 1;

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();





                                 Sleep(30000);
                                 this->send_message(group, question2);
                                 s = 2;


                        }
                        else if (phrase == answer2 && s == 2){



                                     this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question3);
                                 s = 3;


                        }
                        else if (phrase == answer3 && s == 3){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);


                                 this->send_message(group, question4);
                                 s = 4;


                        }
                        else if (phrase == answer4 && s == 4){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question5);
                                 s = 5;


                        }
                        else if (phrase == answer5 && s == 5){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question6);
                                 s = 6;


                        }
                        else if (phrase == answer6 && s == 6){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question7);
                                 s = 7;


                        }
                        else if (phrase == answer7 && s == 7){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question8);
                                 s = 8;


                        }
                        else if (phrase == answer8 && s == 8){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question9);
                                 s = 9;


                        }
                        else if (phrase == answer9 && s == 9){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question10);
                                 s = 10;


                        }
                        else if (phrase == answer10 && s == 10){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question11);
                                 s = 11;


                        }
                        else if (phrase == answer11 && s == 11){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question12);
                                 s = 12;


                        }
                        else if (phrase == answer12 && s == 12){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question13);
                                 s = 13;


                        }
                        else if (phrase == answer13 && s == 13){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question14);
                                 s = 14;


                        }
                        else if (phrase == answer14 && s == 14){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question15);
                                 s = 15;


                        }
                        else if (phrase == answer15 && s == 15){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question16);
                                 s = 16;


                        }
                        else if (phrase == answer16 && s == 16){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question17);
                                 s = 17;


                        }
                        else if (phrase == answer17 && s == 17){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question18);
                                 s = 18;


                        }
                        else if (phrase == answer18 && s == 18){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question19);
                                 s = 19;


                        }
                        else if (phrase == answer19 && s == 19){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question20);
                                 s = 20;


                        }
                        else if (phrase == answer20 && s == 20){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question21);
                                 s = 21;


                        }
                        else if (phrase == answer21 && s == 21){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question22);
                                 s = 22;


                        }
                        else if (phrase == answer22 && s == 22){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question23);
                                 s = 23;


                        }
                        else if (phrase == answer23 && s == 23){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question24);
                                 s = 24;


                        }
                        else if (phrase == answer24 && s == 24){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question25);
                                 s = 25;


                        }
                        else if (phrase == answer25 && s == 25){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question26);
                                 s = 26;


                        }
                        else if (phrase == answer26 && s == 26){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question27);
                                 s = 27;


                        }
                        else if (phrase == answer27 && s == 27){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question28);
                                 s = 28;


                        }
                        else if (phrase == answer28 && s == 28){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question29);
                                 s = 29;


                        }
                        else if (phrase == answer29 && s == 29){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question30);
                                 s = 30;


                        }
                        else if (phrase == answer30 && s == 30){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question31);
                                 s = 31;


                        }
                        else if (phrase == answer31 && s == 31){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question32);
                                 s = 32;


                        }
                        else if (phrase == answer32 && s == 32){


                                 this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question33);
                                 s = 33;


                        }
                        else if (phrase == answer33 && s == 33){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question34);
                                 s = 34;


                        }
                        else if (phrase == answer34 && s == 34){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(30000);
                                 this->send_message(group, question35);
                                 s = 35;


                        }
                        else if (phrase == answer35 && s == 35){


                                this->send_message(group, palDB.userLookUp(user)+" That is correct");


                                      string player = "\n"+palDB.userLookUp(user);

                                      ofstream triviaPlayers;
                                      triviaPlayers.open ("triviascore.bin", ios::out | ios::app );
                                      triviaPlayers << player ;
                                      triviaPlayers.close();


                                 Sleep(5000);
                                 this->send_message(group, "Final Score Totals: \n"+calculateTriviaScore());
                                 this->send_message(group, "Thank you for playing come back again for trivia! \n");
                                 s = 0;


                        }



                        else if(phrase == "hi rachel")
                        {
                                this->send_message(group, "/me Rachel is the best ginger ever!!");
                        }
                        else if(phrase == "hi spud")
                        {
                                this->send_message(group, "/me Spud is a better ginger than Rachel!!");
                        }
                        else if(phrase == "hi carly")
                        {
                                this->send_message(group, "/me we are supposed to ignore her shhhh!");
                        }//*/


                        else /* word filters, triggers if the word is anywhere in the users post */
                        {
                                //word trigger
                                //remember to break at the end of each if
                                //for the love of god dont forget

                                for(int i=0; i<blocks; i++)
                                {
                                        //Hash Tags, triggers if someone uses a hashtag
                                        if(i==(blocks-1) && engine.str2ch(data[i])[0] == '#')
                                        {
                                                string hash = data[i];
                                                if(hash == "#poniproblems")
                                                {
                                                        this->send_message(group, "Forget you, and forget your ponies");
                                                        //this->admin_silence(group, user);
                                                }
                                                else if(hash == "#yolo")
                        {
                            this->send_message(group, "BISH\r\nYou only live once\r\nSo stop getting drunk every day because YOU HAVE A LIFE TO LIVE BISH");
                        }

                                                else if(hash == "#swag")
                                                {
                                                        this->send_message(group, "Man check you out, friggin swaggin so hard, the term swag itself comes from how swaggin you are.");
                                                }

                                                else if(hash == "#whatgirlssay")
                                                {
                                                        this->send_message(group, "You think your so cool for using hashtags on Palringo. #yournotcool #loser #whohashtagsonpal #hashtagsfortwitteronly");
                                                }



                                                break;
                                        }
                                        else if( searchMod(data[i]) != "" )
                    {
                        string output;
                        string output2;
                        string id;
                        string modid;

                        //save to a temp botMod instance so we dont have to keep searching
                        botMod temp = botMods.find(searchMod(data[i]))->second;

                        //botMods.find(searchMod(data[i]))->first;


                        if(temp.online)
                        {
                            output += data[i]+" is online!";
                            //this->send_pm(temp.id, "you are being summoned to " +botGroupName );
                            //this->send_pm(botAdmin, temp.name+" is being summoned to "+botGroupName );






                        }
                        else
                        {
                            output += data[i]+" is offline!";

                            if(temp.message != "")
                                output += "\r\nBut they left of a message!\r\n"+temp.message;
                            //this->send_pm(botAdmin, temp.name+" is offline but being summoned to "+botGroupName);



                        }

                        //this->send_message(group, output);

                        break;
                    }

                                        /*else if(data[i] == "u" || data[i] == "c" || data[i] == "r" || data[i] == "y" || data[i] == "o")
                                        {
                                                this->send_message(group, "Use words not letters\r\nDon't be lazy!");
                                                //this->admin_silence(group, user);
                                                break;
                                        }*/
                                        //favs stuff
                                        /*else if(data[i] == "friend"||data[i] == "mod")
                                        {
                                                this->send_message(group, "Bring a friend, be a mod!");

                                                break;
                                        }
                                        else if(data[i] == "fistpump" || data[i] == "fist" || data[i] == "pump")
                                        {
                                                this->send_message(group, "This aint jersey shore snookie!!");

                                                break;
                                        }
                                        else if(data[i] == "prolly")
                                        {
                                                this->send_message(group, "ooooh");
                                                this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "nigger"||data[i] == "nigga")
                                        {
                                                this->send_message(group, "racist much");
                                                this->admin_kick(group, user);
                                                break;
                                        }*/





                                        //knucklehead stuff
                                        else if(data[i] == "bbl" || data[i] == "bbs" || data[i] == "brb")
                                        {
                                            srand( time(0) ); //seed random number generator
                                            string returnmsg[5] = { "HB!! we are going to miss you!", "Don't leave me with them :s", "Did someone say something", "Take your time we wont notice :P", "Come back soon!!"};
                                            int randomName = rand() %5; //return a number from [0,5)
                                            this->send_message(group, returnmsg[randomName]);
                                                //this->send_message(group, "HB!! we are going to miss you!");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "night" || data[i] == "bed" || data[i] == "bedtime"||data[i] == "goodnight")

                                        {
                                                this->send_message(group, "good night sweet dreams dont let the bed bugs bite, if they do pick up a shoe and knock them cookoo");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        /*else if(data[i] == "hush")
                                        {
                                                this->send_message(group, "no how about you Hush (6)");
                                                this->admin_silence(group, user);
                                                Sleep(10000);
                                                this->admin_reset(group, user);
                                                break;
                                        }*/

                                        /*else if(data[i] == "habit" || data[i] == "habits" || data[i] == "habitual")
                                        {
                                                this->send_message(group, "Old habits die hard!!");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "nothing")
                                        {
                                                this->send_message(group, "nothing ventured, nothing gained");
                                                //this->admin_silence(group, user);
                                                break;
                                        }



                                        else if(data[i] == "bird" || data[i] == "worm" || data[i] == "early")
                                        {
                                                this->send_message(group, "the early bird gets the worm");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "glass" || data[i] == "house" || data[i] == "stone")
                                        {
                                                this->send_message(group, "people who live in glass houses shouldn't throw stones");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "cook" || data[i] == "broth" || data[i] == "spoil")
                                        {
                                                this->send_message(group, "too many cooks spoil the broth");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "will")
                                        {
                                                this->send_message(group, "where there's a will there's a way");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "need" || data[i] == "indeed")
                                        {
                                                this->send_message(group, "a friend in need is a friend indeed");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "horse" || data[i] == "water" || data[i] == "drink")
                                        {
                                                this->send_message(group, "you can lead a horse to water but you cant make him drink");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "gift" || data[i] == "mouth")
                                        {
                                                this->send_message(group, "don't look a gift horse in the mouth");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "smoke" || data[i] == "fire")
                                        {
                                                this->send_message(group, "there's no smoke without fire");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "absence" || data[i] == "heart")
                                        {
                                                this->send_message(group, "Absence makes the heart grow fonder");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                         else if(data[i] == "dead" || data[i] == "died" || data[i] == "killed")
                                        {
                                                this->send_message(group, "Did you Kill it?\r\nWhy dont you be a doll and get it goin again!");
                                                //this->admin_silence(group, user);
                                                break;
                                        }

                                        else if(data[i] == "ass" || data[i] == "asshole" || data[i] == "asswipe")
                                        {
                                                this->send_message(group, "haha your the Ass!! eeeeeeyyyyyyyyahhhhh!!!");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "bitch" || data[i] == "bitches" || data[i] == "son of a bitch")
                                        {
                                                this->send_message(group, "you dont look like a female dog to me!! woof woof bishes");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "fuck" || data[i] == "fuckers" || data[i] == "fucker")
                                        {
                                                this->send_message(group, "Naughty Naughty wash your mouth out with soap");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "damn" || data[i] == "dammit" || data[i] == "damn it")
                                        {
                                                this->send_message(group, "Son of a bish you trying to take the beavers job, only beavers damn it!!");
                                                //this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "cunt" || data[i] == "cunts")
                                        {
                                                this->send_message(group, "thats the worst one you can say thats not nice :@");
                                                this->admin_silence(group, user);
                                                break;
                                        }
                                        else if(data[i] == "shit" || data[i] == "shitty")
                                        {
                                                this->send_message(group, "ewwwww u stink i can smell the crap your talking");
                                                //this->admin_silence(group, user);
                                                break;
                                        }

                                        else if(data[i] == "cod" || (i<blocks-2 && data[i] == "call" && data[i+1] == "of" && data[i+2] == "duty"))
                                        {
                                                /* can also detect phrases anywhere in the post */
                                                /*if(user != botAdmin)
                                                {
                                                        this->send_message(group,       "it would be awesome if you didnt talk about that shitty game\r\n"
                                                                                                                "It has the most inaccurate presntations of firearms second only to doom\r\n"
                                                                                                                "Brink has more accurate weapons than COD\r\n"
                                                                                                                "The ak74 isnt a fucking SMG");
                                                        break;
                                                }
                                        }*/
                                }
                        }
                }
        }

}

//seperate command parser for pms
void baseClient::parse_personalMessage(string name, string user, vector<string> data)
{
        int             blocks          = data.size();
        string  mesg        = this->messagePatcher(data, " ", 1);

    string cmd;
    if(blocks >= patchStart-1)
    {
        if(this->nameSpace != "UNKNOWN")
        {
            cmd = data[0] + " ";

            if(cmd == cmdAdmin || cmd == cmdBase)
            {
               cmd = data[0] + " "+ data[1];
            }
            else
            {
                cmd = data[0];
            }
        }
        else
        {
            cmd = data[0];
        }
    }
    else
    {
        cmd = data[0];
    }

    if(user == botAdmin or isMod(user))
    {
        if(cmd == cmdAdmin+"msg")
        {
            if(blocks > patchStart)
            {
                //generate message
                string buff = this->messagePatcher(data, " ", patchStart+1);
                string user = data[patchStart-1];

                //send the buffer
                this->send_pm(user, buff);
            }
            else
            {
                this->send_pm(user, cmdAdmin+"msg <user ID> <message>");
            }
        }
        if(cmd == cmdAdmin+"uptime")
        {
            double uptime = difftime(time(NULL), startTime);

            char buffer[256];
            int days    = uptime / 86400;
            int hours   = (uptime - 86400 * days) / 3600;
            int minutes = ((uptime - 86400 * days) - (3600 * hours))/60;
            int seconds = ((uptime - 86400 * days) - (3600 * hours))- 60 * minutes;

            //double days = (double)uptime/86400;
            snprintf(buffer, sizeof(buffer), "Uptime: %id %ih %im %is", days, hours, minutes, seconds);

            this->send_pm(user, buffer);
        }
        if(cmd == cmdAdmin+"leave")
        {
            if(blocks == patchStart)
            {
                this->group_part(data[patchStart-1]);
            }
            else
            {
                this->send_pm(user, cmdAdmin+"leave <group id>");
            }
        }
        if(cmd == cmdAdmin+"join")
        {
            if(blocks >= patchStart)
            {
                if(mesg.find("[") != std::string::npos && mesg.find("]") != std::string::npos)
                {
                    //buffer to save string to
                    char const* buffer = mesg.c_str();
                    int length = strlen(buffer);

                    string groupName;
                    string password;
                    int sPos = 0;
                    int ePos = 0;

                    //loop through the string and find the location of brackets
                    for(int i=0; i < length; i++)
                    {
                        if(buffer[i] == '[')
                            sPos = i+1;

                        if(buffer[i] == ']')
                            ePos = i;

                        if(ePos != 0)
                            break;
                    }

                    //if both brackets exit do some things
                    if(ePos != 0)
                    {
                        //set group name
                        groupName = mesg.substr(sPos, ePos-sPos);

                        //find password
                        if((sPos != 0 and ePos != 0) and (buffer[ePos+1] == ' '))
                            password = mesg.substr(ePos+2, length);
                        else
                            password = "";

                        //join the group
                        this->send_pm(user, "Joining: "+groupName);
                        this->group_join(groupName, password);

                    }
                }
                else
                {
                    string groupName = this->messagePatcher(data, " ", patchStart);

                    this->send_pm(user, "Joining: "+groupName);
                    this->group_join(groupName);
                }
            }
            else
            {
                this->send_pm(user, cmdAdmin+"join [<group name>] <password>");
            }
        }
        if(cmd == cmdAdmin+"kick")
        {
            if(blocks > patchStart-1)
            {
                //generate message

                string user = this->messagePatcher(data, " ", patchStart);

                //send the buffer

                this->admin_kick(botGroupID, user);

            }

        }
        if(cmd == cmdAdmin+"help")
        {
            this->send_pm(user,         "Admin Help\r\n"+
                                    cmdAdmin+"msg <user id> <message>\r\n"+
                                    cmdAdmin+"join [<group name>] <password>\r\n"+
                                    cmdAdmin+"leave <group id>\r\n"+
                                    cmdAdmin+"Kick <user id>\r\n"+
                                    cmdAdmin+"uptime\r\n"
                                    );
        }
    }
    else
    {
        string buffer = "";
                buffer.append("====== PM Received ======\r\n");
                buffer.append("From: ");
                buffer.append(name);
                buffer.append("\r\n");
                buffer.append(mesg);
                buffer.append("\r\n=======================\r\n");
                this->send_pm(botAdmin, buffer); //sends the botadmin the pm it received


                buffer = "";
                buffer.append("#db msg ");
                buffer.append(user);
                buffer.append(" ");
                this->send_pm(botAdmin, buffer); //sends a second message so the admin can copy/paste the command for eaiser replies
    }
}

//BETA
//handle response packets
void baseClient::parseResponse(packet response, packet sent)
{
    string resp_what = response.search_headers("WHAT");
    string resp_code = engine.i2s(engine.hex2Int(response.getPayload().substr (12,4)));

    //debug info
    if(engine.DEBUG)
    {
        string buffer = "What: "+resp_what+"\r\nCode: "+resp_code;

        cout << "------------------------------------" << endl;
        cout << buffer << endl;
        cout << "------------------------------------" << endl;
    }

    //messages
    if(resp_what == "11")
    {
        if(resp_code == "13")
        {
            this->send_pm(botAdmin, "Failed sending message\r\n\r\nType: "+sent.search_headers("mesg-target")+"\r\nTarget: "+sent.search_headers("target-id")+"\r\nMessage: "+sent.getPayload());
        }
    }

    //Joining Group
    if(resp_what == "1")
    {
        if(resp_code == "2")
        {
            this->send_pm(botAdmin, "Failed joining ["+ sent.search_headers("Name") +"]\r\n\r\ndoes not exist");
        }
        else if(resp_code == "13")
        {
            this->send_pm(botAdmin, "Failed joining ["+ sent.search_headers("Name") +"]\r\n\r\nWrong/Missing password");
        }
        else if(resp_code == "18")
        {
            this->send_pm(botAdmin, "Failed joining ["+ sent.search_headers("Name") +"]\r\n\r\nBanned");
        }
        else
        {
            this->send_pm(botAdmin, "Joined [" + sent.search_headers("Name") + "]");
        }
    }

    //leave group
    if(resp_what == "4")
    {
        if(resp_code == "2")
        {
            this->send_pm(botAdmin, "Failed leaving "+ sent.search_headers("group-id")+"\r\n\r\nNot in group?");
        }
        else if(resp_code == "18")
        {
            this->send_pm(botAdmin, "Failed leaving "+ sent.search_headers("group-id") +"\r\n\r\nBanned");
        }
        else
        {
            this->send_pm(botAdmin, "Left Group "+ sent.search_headers("group-id"));
        }
    }

    //admin actions
    if(resp_what == "19")
    {
        if(resp_code == "13")
        {
            this->send_pm(botAdmin, "Admin action failed\r\n\r\nGroup: "+sent.search_headers("group-id")+"\r\nAction: "+sent.search_headers("Action")+"\r\nTarget: "+sent.search_headers("target-id"));
        }
    }

}

////////////////////////////////////////////////////////////
/* Everything Below This Segmet should remain un touched */
//////////////////////////////////////////////////////////
//base group functions
void 	baseClient::group_join(string groupName, string password)	{ palGroup->group_join(groupName, password); }
void	baseClient::group_part(string groupID)						{ palGroup->group_part(groupID); }

//base message handlers
//void 	baseClient::send_message(string group, string message)		{ if(canTalk) palMesg->send_message(TARGET_GROUP, group, message); }

void 	baseClient::send_message(string group, string message)
{
    messagesSent++;
    palMesg->send_message(TARGET_GROUP, group, message);
}

void 	baseClient::send_pm(string id, string message) 				{ palMesg->send_message(TARGET_PM, id, message); }

void 	baseClient::send_image(string group, string image)		    { palMesg->send_image(TARGET_GROUP, group, image); }
void 	baseClient::send_image_pm(string user, string image)		{ palMesg->send_image(TARGET_PM, user, image); }

//function to send test packet
//packet can be edited in palringoPacket.cpp under ::debug
void    baseClient::send_debug(string to)                           { palMesg->send_debug(TARGET_GROUP, to); }

//base admin functions
void	baseClient::admin_admin  (string groupID, string userID)	{ palGroup->admin(ACTION_ADMIN, groupID, userID); }
void	baseClient::admin_mod    (string groupID, string userID)	{ palGroup->admin(ACTION_MOD, groupID, userID); }
void	baseClient::admin_silence(string groupID, string userID)	{ palGroup->admin(ACTION_SILENCE, groupID, userID); }
void	baseClient::admin_reset  (string groupID, string userID)	{ palGroup->admin(ACTION_RESET, groupID, userID); }
void	baseClient::admin_kick   (string groupID, string userID)	{ palGroup->admin(ACTION_KICK, groupID, userID); }
void	baseClient::admin_ban    (string groupID, string userID)	{ palGroup->admin(ACTION_BAN, groupID, userID); }

//sets pointers
void	baseClient::set_palMesg(palringoMessage *mesg)				{ palMesg = mesg; }
void	baseClient::set_palGroup(palringoGroup *group)				{ palGroup = group; }
void	baseClient::set_palContact(palringoContact *contact)		{ palContact = contact; }

//gets pointers
palringoContact *baseClient::get_palContact()                      { return palContact; }

//public functions to get variables
string 	baseClient::get_Username() 									{ return this->username; }
string 	baseClient::get_Password() 									{ return this->password; }

//various usefull functions

//we start counting at 1 to make out lives eaiser, since vectors.size starts at 1
string baseClient::messagePatcher(vector<string> message, string patch, int start)
{
	string output = "";
	int	   blocks = message.size();

	if(blocks < start)
	{
		for(int i=0; i<blocks; i++)
		{
			output.append(message[i]);
			if(i != blocks-1)
				output.append(patch);
		}
	}
	else if(blocks > start )
	{
		for(int i=start-1; i<blocks; i++)
		{
			output.append(message[i]);
			if(i != blocks-1)
				output.append(patch);
		}

	}
	else if(blocks == start)
	{
		output.append(message[start-1]);
	}
	else
	{
		output.append("null");
	}

	//cout << "Message patcher: " << output << endl;

	return output;
}

//New security functions
bool baseClient::isMod(string userid)
{
    if(botMods.find(userid) != botMods.end())
        return true;
    else
        return false;
}

bool baseClient::isPlayer(string userid)
{
    if(botPlayers.find(userid) != botPlayers.end())
        return true;
    else
        return false;
}

string baseClient::searchPlayer(string playername)
{
    for(std::map<string, botPlayer>::iterator i = botPlayers.begin(); i != botPlayers.end(); ++i)
    {
        if(i->second.name == playername)
            return i->second.id;
    }

    return "";
}

string baseClient::searchMod(string modname)
{
    for(std::map<string, botMod>::iterator i = botMods.begin(); i != botMods.end(); ++i)
    {
        if(i->second.name == modname)
            return i->second.id;
    }

    return "";
}

bool baseClient::isPest(string userid)
{
    int listSize = botPests.size();

    if(listSize > 0)
    {
        for(int i=0; i<listSize; i++)
        {
            if(botPests[i] == userid)
            {
                return true;
            }
        }
    }

    return false;
}

bool baseClient::shouldMute(string userid)
{
    int listSize = muteList.size();

    if(listSize > 0)
    {
        for(int i=0; i<listSize; i++)
        {
            if(muteList[i] == userid)
            {
                return true;
            }
        }
    }

    return false;
}

bool baseClient::shouldBan(string userid)
{
    int listSize = banList.size();

    if(listSize > 0)
    {
        for(int i=0; i<listSize; i++)
        {
            if(banList[i] == userid)
            {
                return true;
            }
        }
    }

    return false;
}

void baseClient::reloadIni(string group)
{
    INIReader reader = INIReader(this->iniFile);
    INIReader triviaPlayersId = INIReader(this->iniFile1);
    INIReader triviaPlayersName = INIReader(this->iniFile2);
    string postBuffer = "INILoader Details"; //so we can stop posting multiple messages

    //change command and namespace
    string botName  = reader.Get("SETTINGS", "botName", "UNKNOWN");
    string nameSpace= reader.Get("SETTINGS", "nameSpace", "UNKNOWN");
    string cmdAdmin = reader.Get("SETTINGS", "cmdAdmin", "#");
    string cmdBase  = reader.Get("SETTINGS", "cmdUser", "/");

    if(botName != this->botName && botName != "UNKNOWN")
    {
        this->botName = botName;
        postBuffer += "\r\nUpdated BotName: "+this->botName;
    }

    if(nameSpace != this->nameSpace)
    {
        if(nameSpace == "UNKNOWN")
        {
            //removed
            postBuffer += "\r\nRemoved namespace";

            this->nameSpace  = "UNKNOWN";
            this->patchStart = 2;
            nameSpace = "";
        }
        else
        {
            //updated
            postBuffer += "\r\nUpdated namespace: "+nameSpace;

            this->nameSpace  = nameSpace;
            this->patchStart = 3;
            nameSpace += " ";

        }
    }
    else
    {
        nameSpace += " ";
    }

    if(cmdAdmin+this->nameSpace+" " != this->cmdAdmin)
    {
        if(this->nameSpace != "UNKNOWN")
        {
            this->cmdAdmin = cmdAdmin+nameSpace;
            postBuffer += "\r\nUpdated Admin Command: "+this->cmdAdmin;
        }
        else
        {
            this->cmdAdmin = cmdAdmin;
        }
    }

    if(cmdBase+this->nameSpace+" " != this->cmdBase)
    {
        if(this->nameSpace != "UNKNOWN")
        {
            this->cmdBase = cmdBase+nameSpace;
            postBuffer += "\r\nUpdated User Command: "+this->cmdBase;
        }
        else
        {
            this->cmdBase = cmdBase;
        }
    }

    /////Mod Functions
    botMods.clear(); //reset the list so we can make changes
    const string modNumbersInput = palDB.modIdLookUp();//string modNumbersInput = reader.Get("MODS", "modAccounts", "null");//const string modNumbersInput = palDB.modIdLookUp();
    const string modNamesInput = palDB.modNameLookUp();//string modNamesInput = reader.Get("MODS", "modNames", "null");//const string modNamesInput = palDB.modNameLookUp();

    vector<string> modNumbers;
    vector<string> modNames;









    /////Mod Functions
    botPlayers.clear(); //reset the list so we can make changes
    ifstream a("id.txt");
    ifstream b("name.txt");
    string id;
    string names;

    a.seekg(0, std::ios::end);
    id.reserve(a.tellg());
    a.seekg(0, std::ios::beg);

    id.assign((std::istreambuf_iterator<char>(a)),
            std::istreambuf_iterator<char>());
    b.seekg(0, std::ios::end);
    names.reserve(b.tellg());
    b.seekg(0, std::ios::beg);

    names.assign((std::istreambuf_iterator<char>(b)),
            std::istreambuf_iterator<char>());
    /*ofstream idout;
    idout.open ("triviaPlayersId.bin", ios::out  );
    idout << id ;
    idout.close();
    ofstream nameout;
    nameout.open ("triviaplayersName.bin", ios::out  );
    nameout << names ;
    nameout.close();*/

    ifstream t("triviaplayersId.bin");
    ifstream u("triviaplayersName.bin");
    string playerNumbersInput;
    string playerNamesInput;

    t.seekg(0, std::ios::end);
    playerNumbersInput.reserve(t.tellg());
    t.seekg(0, std::ios::beg);

    playerNumbersInput.assign((std::istreambuf_iterator<char>(t)),
            std::istreambuf_iterator<char>());
    u.seekg(0, std::ios::end);
    playerNamesInput.reserve(u.tellg());
    u.seekg(0, std::ios::beg);

    playerNamesInput.assign((std::istreambuf_iterator<char>(u)),
            std::istreambuf_iterator<char>());
    //string playerNumbersInput = triviaPlayersId.Get("PLAYERS", "playerAccounts", "null");//triviaPlayersId.get
    //string playerNamesInput = triviaPlayersName.Get("PLAYERS", "playerNames", "null");//triviaPlayersName.get
    vector<string> playerNumbers;
    vector<string> playerNames;





    if(modNumbersInput != "null")
        boost::split(modNumbers, modNumbersInput, boost::is_any_of("\t "));//modNumbers = engine.splitStr(modNumbersInput, '|');//boost::split(modNumbers, modNumbersInput, boost::is_any_of("\t "));//

    if(modNamesInput != "null")
        boost::split(modNames, modNamesInput, boost::is_any_of("\t "));// modNumbers = engine.splitStr(modNamesInput, '|');//boost::split(modNames, modNamesInput, boost::is_any_of("\t "));//

    if((modNames.size() == modNumbers.size()))
    {
        int modCount = modNumbers.size();
        for(int i=0; i<modCount; i++)
        {

            botMod temp;
            temp.id = modNumbers[i];
            temp.name = modNames[i];

            this->botMods[modNumbers[i]] = temp;
        }

        if(group != "")
            postBuffer += "\r\nMods: "+modNumbersInput;
    }
    else

        postBuffer += "\r\nNo Players Found";
if(playerNumbersInput != "null")
        playerNumbers = engine.splitStr(playerNumbersInput, '|');

    if(playerNamesInput != "null")
        playerNames = engine.splitStr(playerNamesInput, '|');

    if((playerNames.size() == playerNumbers.size()))
    {
        int playerCount = playerNumbers.size();
        for(int i=0; i<playerCount; i++)
        {
            botPlayer temp;
            temp.id = playerNumbers[i];
            temp.name = playerNames[i];

            this->botPlayers[playerNumbers[i]] = temp;
        }

        if(group != "")
            postBuffer += "\r\nTrivia Players: "+playerNumbersInput;
    }
    else
        postBuffer += "\r\nNo Players Found";
    /////Pest Functions
    botPests.clear(); //reset the list so we can make changes
    string pestInput = reader.Get("SECURITY", "ignore", "null");

    if(pestInput != "null" and pestInput != "")
    {
        botPests = engine.splitStr(pestInput, '|');

        if(group != "")
            postBuffer += "\r\nPests: "+pestInput;
    }
    else
        postBuffer += "\r\nNo pests found";

    /////Mute Functions
    muteList.clear(); //reset the list so we can make changes
    string muteInput = reader.Get("SECURITY", "autoMute", "null");

    if(muteInput != "null" and muteInput != "")
    {
        muteList = engine.splitStr(muteInput, '|');

        if(group != "")
            postBuffer += "\r\nMute List: "+muteInput;
    }
    else
        postBuffer += "\r\nNo Mutes Found";

    /////Ban Functions
    banList.clear(); //reset the list so we can make changes
    string banInput = reader.Get("SECURITY", "autoBan", "null");

    if(banInput != "null" and banInput != "")
    {
        banList = engine.splitStr(banInput, '|');

        if(group != "")
            postBuffer += "\r\nBan List: "+banInput;
    }
    else
        postBuffer += "\r\nNo Bans Found";

    //send message buffer
    if(group != "")
        this->send_message(group, postBuffer);
}
void preprocess_input( std::string &str )
{
	cleanString(str);
	UpperCase(str);
}

// make a search for the user's input
// inside the database of the program
vstring find_match(std::string input)
{
	vstring result;
	for(int i = 0; i < nKnowledgeBaseSize; ++i)
	{
		std::string keyWord(KnowledgeBase[i].input);
		// there has been some improvements made in
		// here in order to make the matching process
		// a littlebit more flexible
		if( input.find(keyWord) != std::string::npos )
		{
			copy( KnowledgeBase[i].responses, result );
			return result;
		}
	}
	return result;
}

// here too there was some improvement and it was to make the databse less static,
// which means that the program can now easily handle a number of responses
// for the current keyword varying from 0 to 3 responses
void copy(char *array[], vstring &v)
{
	for(int i = 0; i < MAX_RESP; ++i)
	{
		if(array[i] != NULL)
		{
			v.push_back(array[i]);
		}
		else
		{
			break;
		}
	}
}

void UpperCase( std::string &str )
{
	int len = str.length();
	for( int i = 0; i < len; i++ )
	{
		if ( str[i] >= 'a' && str[i] <= 'z' )
		{
			str[i] -= 'a' - 'A';
		}
	}
}

bool isPunc(char c)
{
	return delim.find(c) != std::string::npos;
}

// removes punctuation and redundant
// spaces from the user's input
void cleanString( std::string &str )
{
	int len = str.length();
	std::string temp = "";

	char prevChar = 0;

	for(int i = 0; i < len; ++i)
	{
		if(str[i] == ' ' && prevChar == ' ')
		{
			continue;
		}

		else if(!isPunc(str[i]))
		{
			temp += str[i];
		}

		prevChar = str[i];
	}
	str = temp;
}



